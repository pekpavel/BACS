package DBModels;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Pavel Pek on 23.04.2017.
 */
@Entity
@Table(name = "person", schema = "public", catalog = "db17_pekpavel")
public class PersonEntity {
    private int personid;
    private String email;
    private String employeeId;
    private String firstName;
    private String lastName;
    private String personalId;
    private String phoneNumber;
    private String sex;
    private String active;
    private int loginid;
    private int personAddressid;
    private int cardid;
    private Collection<AccessLogEntity> accessLogsByPersonid;
    private Collection<JoinpersontoaccessEntity> joinpersontoaccessesByPersonid;
    private LoginEntity loginByLoginid;
    private PersonAddressEntity personAddressByPersonAddressid;
    private CardEntity cardByCardid;

    @Id
    @Column(name = "personid")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getPersonid() {
        return personid;
    }

    public void setPersonid(int personid) {
        this.personid = personid;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "employee_id")
    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    @Basic
    @Column(name = "first_name")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "last_name")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "personal_id")
    public String getPersonalId() {
        return personalId;
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }

    @Basic
    @Column(name = "phone_number")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Basic
    @Column(name = "sex")
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Basic
    @Column(name = "active")
    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    @Basic
    @Column(name = "loginid")
    public int getLoginid() {
        return loginid;
    }

    public void setLoginid(int loginid) {
        this.loginid = loginid;
    }

    @Basic
    @Column(name = "person_addressid")
    public int getPersonAddressid() {
        return personAddressid;
    }

    public void setPersonAddressid(int personAddressid) {
        this.personAddressid = personAddressid;
    }

    @Basic
    @Column(name = "cardid")
    public int getCardid() {
        return cardid;
    }

    public void setCardid(int cardid) {
        this.cardid = cardid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PersonEntity that = (PersonEntity) o;

        if (personid != that.personid) return false;
        if (loginid != that.loginid) return false;
        if (personAddressid != that.personAddressid) return false;
        if (cardid != that.cardid) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (employeeId != null ? !employeeId.equals(that.employeeId) : that.employeeId != null) return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (personalId != null ? !personalId.equals(that.personalId) : that.personalId != null) return false;
        if (phoneNumber != null ? !phoneNumber.equals(that.phoneNumber) : that.phoneNumber != null) return false;
        return sex != null ? sex.equals(that.sex) : that.sex == null;
    }

    @Override
    public int hashCode() {
        int result = personid;
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (employeeId != null ? employeeId.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (personalId != null ? personalId.hashCode() : 0);
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * result + (sex != null ? sex.hashCode() : 0);
        result = 31 * result + loginid;
        result = 31 * result + personAddressid;
        result = 31 * result + cardid;
        return result;
    }

    @OneToMany(mappedBy = "personByPersonid")
    public Collection<AccessLogEntity> getAccessLogsByPersonid() {
        return accessLogsByPersonid;
    }

    public void setAccessLogsByPersonid(Collection<AccessLogEntity> accessLogsByPersonid) {
        this.accessLogsByPersonid = accessLogsByPersonid;
    }

    @OneToMany(mappedBy = "personByPersonid")
    public Collection<JoinpersontoaccessEntity> getJoinpersontoaccessesByPersonid() {
        return joinpersontoaccessesByPersonid;
    }

    public void setJoinpersontoaccessesByPersonid(Collection<JoinpersontoaccessEntity> joinpersontoaccessesByPersonid) {
        this.joinpersontoaccessesByPersonid = joinpersontoaccessesByPersonid;
    }

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinColumn(name = "loginid", referencedColumnName = "loginid", nullable = false, insertable= false, updatable = false)
    public LoginEntity getLoginByLoginid() {
        return loginByLoginid;
    }

    public void setLoginByLoginid(LoginEntity loginByLoginid) {
        this.loginByLoginid = loginByLoginid;
    }

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinColumn(name = "person_addressid", referencedColumnName = "person_addressid", nullable = false, insertable= false, updatable = false)
    public PersonAddressEntity getPersonAddressByPersonAddressid() {
        return personAddressByPersonAddressid;
    }

    public void setPersonAddressByPersonAddressid(PersonAddressEntity personAddressByPersonAddressid) {
        this.personAddressByPersonAddressid = personAddressByPersonAddressid;
    }

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinColumn(name = "cardid", referencedColumnName = "cardid", nullable = false, insertable= false, updatable = false)
    public CardEntity getCardByCardid() {
        return cardByCardid;
    }

    public void setCardByCardid(CardEntity cardByCardid) {
        this.cardByCardid = cardByCardid;
    }
}
