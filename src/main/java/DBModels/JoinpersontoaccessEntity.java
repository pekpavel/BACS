package DBModels;

import javax.persistence.*;

/**
 * Created by Pavel Pek on 23.04.2017.
 */
@Entity
@Table(name = "joinpersontoaccess", schema = "public", catalog = "db17_pekpavel")
public class JoinpersontoaccessEntity {
    private int accessid;
    private int personid;
    private int id;
    private AccessEntity accessByAccessid;
    private PersonEntity personByPersonid;

    @Basic
    @Column(name = "accessid")
    public int getAccessid() {
        return accessid;
    }

    public void setAccessid(int accessid) {
        this.accessid = accessid;
    }

    @Basic
    @Column(name = "personid")
    public int getPersonid() {
        return personid;
    }

    public void setPersonid(int personid) {
        this.personid = personid;
    }

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JoinpersontoaccessEntity that = (JoinpersontoaccessEntity) o;

        if (accessid != that.accessid) return false;
        if (personid != that.personid) return false;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        int result = accessid;
        result = 31 * result + personid;
        result = 31 * result + id;
        return result;
    }

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinColumn(name = "accessid", referencedColumnName = "accessid", nullable = false, insertable= false, updatable = false)
    public AccessEntity getAccessByAccessid() {
        return accessByAccessid;
    }

    public void setAccessByAccessid(AccessEntity accessByAccessid) {
        this.accessByAccessid = accessByAccessid;
    }

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinColumn(name = "personid", referencedColumnName = "personid", nullable = false, insertable= false, updatable = false)
    public PersonEntity getPersonByPersonid() {
        return personByPersonid;
    }

    public void setPersonByPersonid(PersonEntity personByPersonid) {
        this.personByPersonid = personByPersonid;
    }
}
