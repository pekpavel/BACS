package DBModels;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


/**
 * Created by Pavel Pek on 23.04.2017.
 */
@Entity
@Table(name = "access_log", schema = "public", catalog = "db17_pekpavel")
public class AccessLogEntity {
    static SimpleDateFormat formatIN = new SimpleDateFormat("dd.MM.yy");
    static DateFormat formatOUT = new SimpleDateFormat("EE", Locale.US);
    String input_date;
    private int accessLogid;
    private String accessday;
    private String accesstime;
    private boolean accessresult;
    private int officeroomid;
    private int personid;
    private OfficeroomEntity officeroomByOfficeroomid;
    private PersonEntity personByPersonid;
    //region Variables for transfering date to day
    private String accessdayname;
    //endregion

    /**
     * Transfers date to day. For example 27.04.17 --> Thu and UPDATES the table for this specific ID
     * @param date - not needed, but has to be there, otherwise it does not work.
     */
    public static String convertDateToDay(String date){
        String input_date = date;
        Date dt1= null;

        try {
            dt1 = formatIN.parse(input_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formatOUT.format(dt1);
    }

    @Id
    @Column(name = "access_logid")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getAccessLogid() {
        return accessLogid;
    }

    public void setAccessLogid(int accessLogid) {
        this.accessLogid = accessLogid;
    }

    @Basic
    @Column(name = "accessday")
    public String getAccessday() {
        return accessday;
    }

    public void setAccessday(String accessday) {
        this.accessday = accessday;
    }

    @Basic
    @Column(name = "accesstime")
    public String getAccesstime() {
        return accesstime;
    }

    public void setAccesstime(String accesstime) {
        this.accesstime = accesstime;
    }

    //COLUMN ADDED BY HAND - it is creating day name based on date in accessday
    @Basic
    @Column(name = "accessdayname")
    public String getAccessdayname() {
        return accessdayname;
    }

    /**
     * Transfers date to day. For example 27.04.17 --> Thu and UPDATES the table for this specific ID
     * @param accessday - not needed, but has to be there, otherwise it does not work.
     */
    public void setAccessdayname(String accessday){
        input_date = this.accessday;
        Date dt1= null;

        try {
            dt1 = formatIN.parse(input_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.accessdayname = formatOUT.format(dt1);
    }

    @Basic
    @Column(name = "accessresult")
    public boolean isAccessresult() {
        return accessresult;
    }

    public void setAccessresult(boolean accessresult) {
        this.accessresult = accessresult;
    }

    @Basic
    @Column(name = "officeroomid")
    public int getOfficeroomid() {
        return officeroomid;
    }

    public void setOfficeroomid(int officeroomid) {
        this.officeroomid = officeroomid;
    }

    @Basic
    @Column(name = "personid")
    public int getPersonid() {
        return personid;
    }

    public void setPersonid(int personid) {
        this.personid = personid;
    }

    @ManyToOne
    @JoinColumn(name = "officeroomid", referencedColumnName = "officeroomid", nullable = false, insertable= false, updatable = false)
    public OfficeroomEntity getOfficeroomByOfficeroomid() {
        return officeroomByOfficeroomid;
    }

    public void setOfficeroomByOfficeroomid(OfficeroomEntity officeroomByOfficeroomid) {
        this.officeroomByOfficeroomid = officeroomByOfficeroomid;
    }

    @ManyToOne
    @JoinColumn(name = "personid", referencedColumnName = "personid", nullable = false, insertable= false, updatable = false)
    public PersonEntity getPersonByPersonid() {
        return personByPersonid;
    }

    public void setPersonByPersonid(PersonEntity personByPersonid) {
        this.personByPersonid = personByPersonid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccessLogEntity that = (AccessLogEntity) o;

        if (isAccessresult() != that.isAccessresult()) return false;
        if (getPersonid() != that.getPersonid()) return false;
        if (getAccessday() != null ? !getAccessday().equals(that.getAccessday()) : that.getAccessday() != null)
            return false;
        return getAccesstime() != null ? getAccesstime().equals(that.getAccesstime()) : that.getAccesstime() == null;
    }

    @Override
    public int hashCode() {
        int result = getAccessday() != null ? getAccessday().hashCode() : 0;
        result = 31 * result + (getAccesstime() != null ? getAccesstime().hashCode() : 0);
        result = 31 * result + (isAccessresult() ? 1 : 0);
        result = 31 * result + getPersonid();
        return result;
    }
}
