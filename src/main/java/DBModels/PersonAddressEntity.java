package DBModels;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Pavel Pek on 23.04.2017.
 */
@Entity
@Table(name = "person_address", schema = "public", catalog = "db17_pekpavel")
public class PersonAddressEntity {
    private int personAddressid;
    private String city;
    private String houseNumber;
    private String street;
    private String zipCode;
    private Collection<PersonEntity> peopleByPersonAddressid;

    @Id
    @Column(name = "person_addressid")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getPersonAddressid() {
        return personAddressid;
    }

    public void setPersonAddressid(int personAddressid) {
        this.personAddressid = personAddressid;
    }

    @Basic
    @Column(name = "city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "house_number")
    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    @Basic
    @Column(name = "street")
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Basic
    @Column(name = "zip_code")
    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PersonAddressEntity that = (PersonAddressEntity) o;

        if (personAddressid != that.personAddressid) return false;
        if (city != null ? !city.equals(that.city) : that.city != null) return false;
        if (houseNumber != null ? !houseNumber.equals(that.houseNumber) : that.houseNumber != null) return false;
        if (street != null ? !street.equals(that.street) : that.street != null) return false;
        return zipCode != null ? zipCode.equals(that.zipCode) : that.zipCode == null;
    }

    @Override
    public int hashCode() {
        int result = personAddressid;
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (houseNumber != null ? houseNumber.hashCode() : 0);
        result = 31 * result + (street != null ? street.hashCode() : 0);
        result = 31 * result + (zipCode != null ? zipCode.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "personAddressByPersonAddressid")
    public Collection<PersonEntity> getPeopleByPersonAddressid() {
        return peopleByPersonAddressid;
    }

    public void setPeopleByPersonAddressid(Collection<PersonEntity> peopleByPersonAddressid) {
        this.peopleByPersonAddressid = peopleByPersonAddressid;
    }
}
