package DBModels;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Pavel Pek on 23.04.2017.
 */
@Entity
@Table(name = "card", schema = "public", catalog = "db17_pekpavel")
public class CardEntity {
    private int cardid;
    private String cardnumber;
    private Collection<PersonEntity> peopleByCardid;

    @Id
    @Column(name = "cardid")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getCardid() {
        return cardid;
    }

    public void setCardid(int cardid) {
        this.cardid = cardid;
    }

    @Basic
    @Column(name = "cardnumber")
    public String getCardnumber() {
        return cardnumber;
    }

    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CardEntity that = (CardEntity) o;

        if (cardid != that.cardid) return false;
        return cardnumber != null ? cardnumber.equals(that.cardnumber) : that.cardnumber == null;
    }

    @Override
    public int hashCode() {
        int result = cardid;
        result = 31 * result + (cardnumber != null ? cardnumber.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "cardByCardid", fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    public Collection<PersonEntity> getPeopleByCardid() {
        return peopleByCardid;
    }

    public void setPeopleByCardid(Collection<PersonEntity> peopleByCardid) {
        this.peopleByCardid = peopleByCardid;
    }
}
