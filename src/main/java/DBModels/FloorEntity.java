package DBModels;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Pavel Pek on 23.04.2017.
 */
@Entity
@Table(name = "floor", schema = "public", catalog = "db17_pekpavel")
public class FloorEntity {
    private int floorid;
    private String floornumber;
    private int buildingid;
    private Collection<DepartmentEntity> departmentsByFloorid;
    private BuildingEntity buildingByBuildingid;

    @Id
    @Column(name = "floorid")
    public int getFloorid() {
        return floorid;
    }

    public void setFloorid(int floorid) {
        this.floorid = floorid;
    }

    @Basic
    @Column(name = "floornumber")
    public String getFloornumber() {
        return floornumber;
    }

    public void setFloornumber(String floornumber) {
        this.floornumber = floornumber;
    }

    @Basic
    @Column(name = "buildingid")
    public int getBuildingid() {
        return buildingid;
    }

    public void setBuildingid(int buildingid) {
        this.buildingid = buildingid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FloorEntity that = (FloorEntity) o;

        if (floorid != that.floorid) return false;
        if (buildingid != that.buildingid) return false;
        if (floornumber != null ? !floornumber.equals(that.floornumber) : that.floornumber != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = floorid;
        result = 31 * result + (floornumber != null ? floornumber.hashCode() : 0);
        result = 31 * result + buildingid;
        return result;
    }

    @OneToMany(mappedBy = "floorByFloorid")
    public Collection<DepartmentEntity> getDepartmentsByFloorid() {
        return departmentsByFloorid;
    }

    public void setDepartmentsByFloorid(Collection<DepartmentEntity> departmentsByFloorid) {
        this.departmentsByFloorid = departmentsByFloorid;
    }

    @ManyToOne
    @JoinColumn(name = "buildingid", referencedColumnName = "buildingid", nullable = false, insertable= false, updatable = false)
    public BuildingEntity getBuildingByBuildingid() {
        return buildingByBuildingid;
    }

    public void setBuildingByBuildingid(BuildingEntity buildingByBuildingid) {
        this.buildingByBuildingid = buildingByBuildingid;
    }
}
