package DBModels;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Pavel Pek on 23.04.2017.
 */
@Entity
@Table(name = "access", schema = "public", catalog = "db17_pekpavel")
public class AccessEntity {
    private int accessid;
    private String accessdayallowed;
    private String accesstimefromto;
    private String accesslevel;
    private Collection<JoinpersontoaccessEntity> joinpersontoaccessesByAccessid;

    @Id
    @Column(name = "accessid")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getAccessid() {
        return accessid;
    }

    public void setAccessid(int accessid) {
        this.accessid = accessid;
    }

    @Basic
    @Column(name = "accessdayallowed")
    public String getAccessdayallowed() {
        return accessdayallowed;
    }

    public void setAccessdayallowed(String accessdayallowed) {
        this.accessdayallowed = accessdayallowed;
    }

    @Basic
    @Column(name = "accesstimefromto")
    public String getAccesstimefromto() {
        return accesstimefromto;
    }

    public void setAccesstimefromto(String accesstimefromto) {
        this.accesstimefromto = accesstimefromto;
    }

    @Basic
    @Column(name = "accesslevel")
    public String getAccesslevel() {
        return accesslevel;
    }

    public void setAccesslevel(String accesslevel) {
        this.accesslevel = accesslevel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccessEntity that = (AccessEntity) o;

        if (accessid != that.accessid) return false;
        if (accessdayallowed != null ? !accessdayallowed.equals(that.accessdayallowed) : that.accessdayallowed != null)
            return false;
        if (accesstimefromto != null ? !accesstimefromto.equals(that.accesstimefromto) : that.accesstimefromto != null)
            return false;
        return accesslevel != null ? accesslevel.equals(that.accesslevel) : that.accesslevel == null;
    }

    @Override
    public int hashCode() {
        int result = accessid;
        result = 31 * result + (accessdayallowed != null ? accessdayallowed.hashCode() : 0);
        result = 31 * result + (accesstimefromto != null ? accesstimefromto.hashCode() : 0);
        result = 31 * result + (accesslevel != null ? accesslevel.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "accessByAccessid")
    public Collection<JoinpersontoaccessEntity> getJoinpersontoaccessesByAccessid() {
        return joinpersontoaccessesByAccessid;
    }

    public void setJoinpersontoaccessesByAccessid(Collection<JoinpersontoaccessEntity> joinpersontoaccessesByAccessid) {
        this.joinpersontoaccessesByAccessid = joinpersontoaccessesByAccessid;
    }
}
