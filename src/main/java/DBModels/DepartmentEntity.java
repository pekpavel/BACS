package DBModels;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Pavel Pek on 23.04.2017.
 */
@Entity
@Table(name = "department", schema = "public", catalog = "db17_pekpavel")
public class DepartmentEntity {
    private int departmentid;
    private String name;
    private String departmentnumber;
    private int floorid;
    private FloorEntity floorByFloorid;
    private Collection<OfficeroomEntity> officeroomsByDepartmentid;

    @Id
    @Column(name = "departmentid")
    public int getDepartmentid() {
        return departmentid;
    }

    public void setDepartmentid(int departmentid) {
        this.departmentid = departmentid;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "departmentnumber")
    public String getDepartmentnumber() {
        return departmentnumber;
    }

    public void setDepartmentnumber(String departmentnumber) {
        this.departmentnumber = departmentnumber;
    }

    @Basic
    @Column(name = "floorid")
    public int getFloorid() {
        return floorid;
    }

    public void setFloorid(int floorid) {
        this.floorid = floorid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DepartmentEntity that = (DepartmentEntity) o;

        if (departmentid != that.departmentid) return false;
        if (floorid != that.floorid) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (departmentnumber != null ? !departmentnumber.equals(that.departmentnumber) : that.departmentnumber != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = departmentid;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (departmentnumber != null ? departmentnumber.hashCode() : 0);
        result = 31 * result + floorid;
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "floorid", referencedColumnName = "floorid", nullable = false, insertable= false, updatable = false)
    public FloorEntity getFloorByFloorid() {
        return floorByFloorid;
    }

    public void setFloorByFloorid(FloorEntity floorByFloorid) {
        this.floorByFloorid = floorByFloorid;
    }

    @OneToMany(mappedBy = "departmentByDepartmentid")
    public Collection<OfficeroomEntity> getOfficeroomsByDepartmentid() {
        return officeroomsByDepartmentid;
    }

    public void setOfficeroomsByDepartmentid(Collection<OfficeroomEntity> officeroomsByDepartmentid) {
        this.officeroomsByDepartmentid = officeroomsByDepartmentid;
    }
}
