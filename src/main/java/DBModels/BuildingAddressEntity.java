package DBModels;

import javax.persistence.*;

/**
 * Created by Pavel Pek on 23.04.2017.
 */
@Entity
@Table(name = "building_address", schema = "public", catalog = "db17_pekpavel")
public class BuildingAddressEntity {
    private int buildingAddressid;
    private String city;
    private String houseNumber;
    private String street;
    private String zipCode;
    private int buildingid;
    private BuildingEntity buildingByBuildingid;

    @Id
    @Column(name = "building_addressid")
    public int getBuildingAddressid() {
        return buildingAddressid;
    }

    public void setBuildingAddressid(int buildingAddressid) {
        this.buildingAddressid = buildingAddressid;
    }

    @Basic
    @Column(name = "city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "house_number")
    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    @Basic
    @Column(name = "street")
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Basic
    @Column(name = "zip_code")
    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Basic
    @Column(name = "buildingid")
    public int getBuildingid() {
        return buildingid;
    }

    public void setBuildingid(int buildingid) {
        this.buildingid = buildingid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BuildingAddressEntity that = (BuildingAddressEntity) o;

        if (buildingAddressid != that.buildingAddressid) return false;
        if (buildingid != that.buildingid) return false;
        if (city != null ? !city.equals(that.city) : that.city != null) return false;
        if (houseNumber != null ? !houseNumber.equals(that.houseNumber) : that.houseNumber != null) return false;
        if (street != null ? !street.equals(that.street) : that.street != null) return false;
        if (zipCode != null ? !zipCode.equals(that.zipCode) : that.zipCode != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = buildingAddressid;
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (houseNumber != null ? houseNumber.hashCode() : 0);
        result = 31 * result + (street != null ? street.hashCode() : 0);
        result = 31 * result + (zipCode != null ? zipCode.hashCode() : 0);
        result = 31 * result + buildingid;
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "buildingid", referencedColumnName = "buildingid", nullable = false, insertable= false, updatable = false)
    public BuildingEntity getBuildingByBuildingid() {
        return buildingByBuildingid;
    }

    public void setBuildingByBuildingid(BuildingEntity buildingByBuildingid) {
        this.buildingByBuildingid = buildingByBuildingid;
    }
}
