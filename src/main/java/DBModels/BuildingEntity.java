package DBModels;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Pavel Pek on 23.04.2017.
 */
@Entity
@Table(name = "building", schema = "public", catalog = "db17_pekpavel")
public class BuildingEntity {
    private int buildingid;
    private String name;
    private Collection<BuildingAddressEntity> buildingAddressesByBuildingid;
    private Collection<FloorEntity> floorsByBuildingid;

    @Id
    @Column(name = "buildingid")
    public int getBuildingid() {
        return buildingid;
    }

    public void setBuildingid(int buildingid) {
        this.buildingid = buildingid;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BuildingEntity that = (BuildingEntity) o;

        if (buildingid != that.buildingid) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = buildingid;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "buildingByBuildingid")
    public Collection<BuildingAddressEntity> getBuildingAddressesByBuildingid() {
        return buildingAddressesByBuildingid;
    }

    public void setBuildingAddressesByBuildingid(Collection<BuildingAddressEntity> buildingAddressesByBuildingid) {
        this.buildingAddressesByBuildingid = buildingAddressesByBuildingid;
    }

    @OneToMany(mappedBy = "buildingByBuildingid")
    public Collection<FloorEntity> getFloorsByBuildingid() {
        return floorsByBuildingid;
    }

    public void setFloorsByBuildingid(Collection<FloorEntity> floorsByBuildingid) {
        this.floorsByBuildingid = floorsByBuildingid;
    }
}
