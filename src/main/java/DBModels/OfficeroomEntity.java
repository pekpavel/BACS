package DBModels;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Pavel Pek on 23.04.2017.
 */
@Entity
@Table(name = "officeroom", schema = "public", catalog = "db17_pekpavel")
public class OfficeroomEntity {
    private int officeroomid;
    private String accesslevelrequired;
    private String name;
    private int departmentid;
    private Collection<AccessLogEntity> accessLogsByOfficeroomid;
    private DepartmentEntity departmentByDepartmentid;

    @Id
    @Column(name = "officeroomid")
    public int getOfficeroomid() {
        return officeroomid;
    }

    public void setOfficeroomid(int officeroomid) {
        this.officeroomid = officeroomid;
    }

    @Basic
    @Column(name = "accesslevelrequired")
    public String getAccesslevelrequired() {
        return accesslevelrequired;
    }

    public void setAccesslevelrequired(String accesslevelrequired) {
        this.accesslevelrequired = accesslevelrequired;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "departmentid")
    public int getDepartmentid() {
        return departmentid;
    }

    public void setDepartmentid(int departmentid) {
        this.departmentid = departmentid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OfficeroomEntity that = (OfficeroomEntity) o;

        if (officeroomid != that.officeroomid) return false;
        if (departmentid != that.departmentid) return false;
        if (accesslevelrequired != null ? !accesslevelrequired.equals(that.accesslevelrequired) : that.accesslevelrequired != null)
            return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = officeroomid;
        result = 31 * result + (accesslevelrequired != null ? accesslevelrequired.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + departmentid;
        return result;
    }

    @OneToMany(mappedBy = "officeroomByOfficeroomid")
    public Collection<AccessLogEntity> getAccessLogsByOfficeroomid() {
        return accessLogsByOfficeroomid;
    }

    public void setAccessLogsByOfficeroomid(Collection<AccessLogEntity> accessLogsByOfficeroomid) {
        this.accessLogsByOfficeroomid = accessLogsByOfficeroomid;
    }

    @ManyToOne
    @JoinColumn(name = "departmentid", referencedColumnName = "departmentid", nullable = false, insertable= false, updatable = false)
    public DepartmentEntity getDepartmentByDepartmentid() {
        return departmentByDepartmentid;
    }

    public void setDepartmentByDepartmentid(DepartmentEntity departmentByDepartmentid) {
        this.departmentByDepartmentid = departmentByDepartmentid;
    }
}
