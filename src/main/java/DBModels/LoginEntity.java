package DBModels;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Pavel Pek on 23.04.2017.
 */
@Entity
@Table(name = "login", schema = "public", catalog = "db17_pekpavel")
public class LoginEntity {
    private int loginid;
    private String username;
    private String password;
    private Collection<PersonEntity> peopleByLoginid;

    @Id
    @Column(name = "loginid")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getLoginid() {
        return loginid;
    }

    public void setLoginid(int loginid) {
        this.loginid = loginid;
    }

    @Basic
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LoginEntity that = (LoginEntity) o;

        if (loginid != that.loginid) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        return password != null ? password.equals(that.password) : that.password == null;
    }

    @Override
    public int hashCode() {
        int result = loginid;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "loginByLoginid", fetch = FetchType.EAGER,cascade=CascadeType.ALL)
    public Collection<PersonEntity> getPeopleByLoginid() {
        return peopleByLoginid;
    }

    public void setPeopleByLoginid(Collection<PersonEntity> peopleByLoginid) {
        this.peopleByLoginid = peopleByLoginid;
    }
}
