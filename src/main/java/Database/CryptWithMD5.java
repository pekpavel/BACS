package Database;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import static Main.App.LOGS_FOLDER;

public class CryptWithMD5 {
    //region LOGGER + filehandler
    private final static Logger LOGGER = Logger.getLogger(CryptWithMD5.class.getName());
    private static MessageDigest md;
    private static FileHandler fileHandler;
    //endregion

    /**
     * Crypts password with MD5 algorithm...it is not safe, but still safer than plain text!!!
     * @param pass - password to hash
     * @return - encrypted String
     */
    public static String cryptWithMD5(String pass){
        try {
            // This block configure the logger with handler and formatter
            fileHandler = new FileHandler(LOGS_FOLDER + "CryptWithMD5.log", true);
            LOGGER.addHandler(fileHandler);
            SimpleFormatter formatter = new SimpleFormatter();
            fileHandler.setFormatter(formatter);
            LOGGER.setLevel(Level.INFO);
            LOGGER.info("<<< Logging of " + CryptWithMD5.class.getName()+ " initiated successfully");

        } catch (SecurityException e) {
            e.printStackTrace();
            LOGGER.log( Level.SEVERE, e.toString(), e); //Always logging exceptions
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.log( Level.SEVERE, e.toString(), e); //Always logging exceptions
        }

        try {
            md = MessageDigest.getInstance("MD5");
            byte[] passBytes = pass.getBytes();
            md.reset();
            byte[] digested = md.digest(passBytes);
            StringBuffer sb = new StringBuffer();
            for(int i=0;i<digested.length;i++){
                sb.append(Integer.toHexString(0xff & digested[i]));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException ex) {
            LOGGER.log( Level.SEVERE, ex.toString(), ex); //Always logging exceptions
        }
        return null;
    }
}