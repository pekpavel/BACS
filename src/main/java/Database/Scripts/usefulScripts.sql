-- MAXING IDs
SELECT setval('access_accessid_seq', (SELECT MAX(accessid) from "access"));
SELECT setval('access_log_access_logid_seq', (SELECT MAX(access_logid) from "access_log"));
SELECT setval('building_buildingid_seq', (SELECT MAX(buildingid) from "building"));
SELECT setval('building_address_building_addressid_seq', (SELECT MAX(building_addressid) from "building_address"));
SELECT setval('card_cardid_seq', (SELECT MAX(cardid) from "card"));
SELECT setval('department_departmentid_seq', (SELECT MAX(departmentid) from "department"));
SELECT setval('floor_floorid_seq', (SELECT MAX(floorid) from "floor"));
SELECT setval('joinpersontoaccess_id_seq', (SELECT MAX(id) from "joinpersontoaccess"));
SELECT setval('login_loginid_seq', (SELECT MAX(loginid) from "login"));
SELECT setval('officeroom_officeroomid_seq', (SELECT MAX(officeroomid) from "officeroom"));
SELECT setval('person_address_person_addressid_seq', (SELECT MAX(person_addressid) from "person_address"));
SELECT setval('person_personid_seq', (SELECT MAX(personid) from "person"));

--TRIGGERS
CREATE OR REPLACE FUNCTION CheckAccess()
  RETURNS TRIGGER
AS $$
DECLARE
  row Access%ROWTYPE;
BEGIN
  FOR row in SELECT * FROM access
  LOOP
    IF((row.accessdayallowed = NEW.accessdayallowed)
       AND (row.accesstimefromto = new.accesstimefromto)
       AND (row.accesslevel = new.accesslevel))
    THEN RAISE EXCEPTION 'Access time already exists.';
	END IF;
  END LOOP;
  RETURN new;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER "CheckAccessTrigger"
BEFORE UPDATE OR INSERT
  ON Access
FOR EACH ROW
EXECUTE PROCEDURE CheckAccess();


-- ADDING COLUMN
ALTER TABLE person ADD COLUMN active CHARACTER(1) NOT NULL DEFAULT '1'
  CHECK (active = '0' or active = '1');

CREATE OR REPLACE FUNCTION delete_card_login()
  RETURNS trigger AS $delete_person$
DECLARE
  row person%ROWTYPE;
BEGIN
  DELETE FROM card
  WHERE cardid = row.cardid;
  DELETE FROM login
  WHERE loginid = row.loginid;
  RETURN old;
END;
$delete_person$ LANGUAGE plpgsql;


CREATE TRIGGER delete_person
BEFORE DELETE ON person
EXECUTE PROCEDURE delete_card_login();

DROP TRIGGER delete_person on person;