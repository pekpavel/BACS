package Database;

import java.io.IOException;
import java.sql.*;
import java.util.Arrays;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import static Database.CryptWithMD5.cryptWithMD5;
import static Main.App.LOGS_FOLDER;

public class Db {

    //region LOGGER + filehandler
    private final static Logger LOGGER = Logger.getLogger(Db.class.getName());
    //region CONNECTION VARIABLES
    private static final String dbURL = "jdbc:postgresql://slon.felk.cvut.cz:5432/db17_pekpavel";
    //endregion
    private static final String dbName = "db17_pekpavel";
    private static final String dbPassword = "admin";
    public static String username;
    private static FileHandler fileHandler;
    //endregion
    private static Connection c = null;
    private static int userID;
    //region RETURN CODES
    private int retConnection = -1; //0 = CONNECTED, -1 = NOT CONNECTED


    public Db(){

    }

    /**
     * Get logged user id
     * @return id of an user
     */
    public static int getLoggedUserID(){
        LOGGER.info("<<< Request for getting logged user's id.");
        return userID;
    }

    /**
     * Get username of an logged user
     * @return username of an user
     */
    public static String getUsername() {
        LOGGER.info("<<< Request for getting logged user's username.");
        return username;
    }
    //endregion

    /**
     * Returns personid from table person based on given loginid
     * @param loginID - loginID of person we want to find
     * @return personID if we found someone, -1 if we did not find anything
     */
    public static int getPersonID(int loginID){
        LOGGER.info("<<< Request for getting person id.");
        int personID;
        PreparedStatement selectPersonID = null;
        String selectQuery =
                "SELECT personid FROM person WHERE loginid ="+loginID;
        try {
            selectPersonID = c.prepareStatement(selectQuery);
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log( Level.SEVERE, e.toString(), e); //Always logging exceptions
        }

        ResultSet result;
        try {
            result = selectPersonID.executeQuery();
            if(result.next()){
                personID = result.getInt(1);
                //CLEAN UP
                result.close();
                selectPersonID.close();
                return personID;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
        }

        //Else return -1
        return -1;
    }

    /**
     * Get adress id from Person_address table
     * @param cityIn - city
     * @param housenumIn - house number
     * @param zipcodeIn - zip code
     * @param streetIn - street
     * @return -1 if not found, else return adress id
     */
    public static int getAddressID(String cityIn, String housenumIn, String zipcodeIn, String streetIn) {
        int adrID;
        PreparedStatement selectadressID = null;
        String selectQuery =
                "SELECT person_addressid FROM person_address WHERE (city='"+cityIn+"' and house_number='"+housenumIn+"' and street='"+streetIn+"' and zip_code='"+zipcodeIn+"')";
        try {
            selectadressID = c.prepareStatement(selectQuery);
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
        }

        ResultSet result;
        try {
            result = selectadressID.executeQuery();
            if(result.next()){
                adrID = result.getInt(1);
                //CLEAN UP
                result.close();
                selectadressID.close();
                return adrID;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
        }

        //Else return -1
        return -1;
    }

    /**
     * Get access id from Access table
     * @param accesslvl - level of access
     * @param accessday - day of access
     * @param accesstime - time of access
     * @return -1 if not found, else return access id
     */
    public static int getAccessID(String accesslvl, String accessday, String accesstime) {
        int accID;
        PreparedStatement selectaccessID = null;
        String selectQuery =
                "SELECT accessid FROM access WHERE (accessdayallowed='"+accessday+"' and accesstimefromto='"+accesstime+"' and accesslevel='"+accesslvl+"')";
        try {
            selectaccessID = c.prepareStatement(selectQuery);
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
        }

        ResultSet result;
        try {
            result = selectaccessID.executeQuery();
            if(result.next()){
                accID = result.getInt(1);
                //CLEAN UP
                result.close();
                selectaccessID.close();
                return accID;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
        }

        //Else return -1
        return -1;
    }

    /**
     * Get card id from Card table
     * @param cardnum - number of card
     * @return -1 if not found, else return card id
     */
    public static int getCardID(String cardnum) {
        int cardID;
        PreparedStatement selectcardID = null;
        String selectQuery =
                "SELECT cardid FROM card WHERE cardnumber='"+cardnum+"'";
        try {
            selectcardID = c.prepareStatement(selectQuery);
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
        }

        ResultSet result;
        try {
            result = selectcardID.executeQuery();
            if(result.next()){
                cardID = result.getInt(1);
                //CLEAN UP
                result.close();
                selectcardID.close();
                return cardID;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
        }

        //Else return -1
        return -1;
    }

    /**
     * Get login id from Login table
     * @param username - username of a person
     * @return -1 if not found, else return login id
     */
    public static int getLoginID(String username) {
        int loginID;
        PreparedStatement selectloginID = null;
        String selectQuery =
                "SELECT loginid FROM login WHERE username='"+username+"'";
        try {
            selectloginID = c.prepareStatement(selectQuery);
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
        }

        ResultSet result;
        try {
            result = selectloginID.executeQuery();
            if(result.next()){
                loginID = result.getInt(1);
                //CLEAN UP
                result.close();
                selectloginID.close();
                return loginID;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
        }

        //Else return -1
        return -1;
    }

    /**
     * Get id from Joinpersontoaccess table
     * @param AccessID - id of an access
     * @param PersonID - id of a person
     * @return -1 if not found, else return joinpersontoaccess id
     */
    public static int getJptaID(int AccessID, int PersonID) {
        int jptaID;
        PreparedStatement selectjptaID = null;
        String selectQuery =
                "SELECT id FROM joinpersontoaccess WHERE (personid='"+PersonID+"' and accessid='"+AccessID+"')";
        try {
            selectjptaID = c.prepareStatement(selectQuery);
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
        }

        ResultSet result;
        try {
            result = selectjptaID.executeQuery();
            if(result.next()){
                jptaID = result.getInt(1);
                //CLEAN UP
                result.close();
                selectjptaID.close();
                return jptaID;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
        }

        //Else return -1
        return -1;
    }

    /**
     * Checks if email is in use by any other user
     * @param email - email of a person
     * @return -1 if not found, else 1
     */
    public static int checkEmailIfExists(String email) {
        PreparedStatement selectemail = null;
        String selectQuery =
                "SELECT * FROM person WHERE (email='"+email+"')";
        try {
            selectemail = c.prepareStatement(selectQuery);
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
        }
        ResultSet result;

        try {
            result = selectemail.executeQuery();
            if(result.next()){
                result.close();
                selectemail.close();
                return 1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
        }

        //Else return -1
        return -1;
    }

    /**
     * Check if employee id is in use by any other user
     * @param employeeid - employee id of a person
     * @return -1 if not found, else 1
     */
    public static int checkEmployeeidIfExists(String employeeid) {
        PreparedStatement selectemail = null;
        String selectQuery =
                "SELECT * FROM person WHERE (employee_id='"+employeeid+"')";
        try {
            selectemail = c.prepareStatement(selectQuery);
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
        }
        ResultSet result;

        try {
            result = selectemail.executeQuery();
            if(result.next()){
                result.close();
                selectemail.close();
                return 1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
        }

        //Else return -1
        return -1;
    }

    /**
     * Check if phone number is in use by any other user
     * @param phonenum - phone number of a person
     * @return -1 if not found, else 1
     */
    public static int checkPhonenumberIfExists(String phonenum) {
        PreparedStatement selectemail = null;
        String selectQuery =
                "SELECT * FROM person WHERE (phone_number='"+phonenum+"')";
        try {
            selectemail = c.prepareStatement(selectQuery);
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
        }
        ResultSet result;

        try {
            result = selectemail.executeQuery();
            if(result.next()){
                result.close();
                selectemail.close();
                return 1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
        }

        //Else return -1
        return -1;
    }

    /**
     * Check if personal id is in use by any other user
     * @param personalid - personal id of a person
     * @return -1 if not found, else 1
     */
    public static int checkPersonalidIfExists(String personalid) {
        PreparedStatement selectemail = null;
        String selectQuery =
                "SELECT * FROM person WHERE (personal_id='"+personalid+"')";
        try {
            selectemail = c.prepareStatement(selectQuery);
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
        }
        ResultSet result;

        try {
            result = selectemail.executeQuery();
            if(result.next()){
                result.close();
                selectemail.close();
                return 1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
        }

        //Else return -1
        return -1;
    }

    /**
     * Checks if signed user is admin
     * USER IS ADMIN IF HIS CARD STARTS WITH 533xxxxx
     * @throws SQLException
     * @return TRUE if user is ADMIN, FALSE if user is not ADMIN
     */
    public static boolean isUserAdmin() throws SQLException{
        LOGGER.info("<<< Checking if USER: >>>"+ getUsername() +"<<< is ADMIN.");

        int cardID;
        String cardNumber;
        PreparedStatement selectCardID = null;

        String selectQuery = "SELECT cardid FROM person WHERE personid =" + getPersonID(getLoggedUserID()); //Select cardid from person table

        selectCardID = c.prepareStatement(selectQuery);
        ResultSet result = selectCardID.executeQuery();

        //After the cardid is found
        if(result.next()){
            cardID = result.getInt(1);
            selectQuery = "SELECT cardnumber FROM card WHERE cardid ="+cardID; //Select cardnumber from cardid
            selectCardID = c.prepareStatement(selectQuery);
            result = selectCardID.executeQuery();
            if(result.next()){
                cardNumber = result.getString(1);
                char[] charArray = cardNumber.toCharArray(); //Convert cardNumber to CharArray
                char[] firstThreeNumbers = new char[3];
                char[] adminNumbers = {'5', '3', '3'};

                //Take first three numbers from cardNumber
                for(int i = 0; i < 3; i++){
                    firstThreeNumbers[i] = charArray[i];
                }

                //Compare them with adminNumbers
                if(Arrays.equals(firstThreeNumbers, adminNumbers)){
                    //CLEAN UP
                    result.close();
                    selectCardID.close();
                    LOGGER.info("<<< USER is ADMIN.");
                    return true; //User is admin
                }
            }
        }

        result.close();
        selectCardID.close();
        LOGGER.info("<<< User is not admin.");
        return false; //User is not admin
    }

    /**
     * Just loads LOGGER
     */
    public void loadLogger(){
        try {
            // This block configure the logger with handler and formatter
            fileHandler = new FileHandler(LOGS_FOLDER + "Db.log", true);
            LOGGER.addHandler(fileHandler);
            SimpleFormatter formatter = new SimpleFormatter();
            fileHandler.setFormatter(formatter);
            LOGGER.setLevel(Level.INFO);
            LOGGER.info("<<< Logging of " +Db.class.getName()+ " initiated successfully");

        } catch (SecurityException e) {
            e.printStackTrace();
            LOGGER.log( Level.SEVERE, e.toString(), e); //Always logging exceptions
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.log( Level.SEVERE, e.toString(), e); //Always logging exceptions
        }
    }

    /**
     * Method visible publicly, but "user" cannot see database login information
     */
    public void openConnection(){
        Connect(dbURL, dbName, dbPassword);
    }

    /**
     * Tries to connect to database based on given parameters
     * SETS retConnection to 0 if CONNECTED, -1 if DISCONNECTED
     * @param dbURL - Database HOST
     * @param dbName - Database NAME
     * @param dbPassword - Database PASSWORD
     */
    private void Connect(String dbURL, String dbName, String dbPassword){

        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection(dbURL, dbName, dbPassword);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
            LOGGER.info("<<< Could not connect to database.");
            retConnection = -1; //NOT CONNECTED
            return;
        }
        LOGGER.info("<<< Opened database successfully.");
        retConnection = 0; //CONNECTED
    }

    /**
     * Tries to close connection to database
     * SETS retConnection to 0 if CONNECTED, -1 if DISCONNECTED
     */
    public void closeConnection() {
        try {
            c.close();
            retConnection = -1; //DISCONNECTED
            LOGGER.info("<<< Closed database successfully.");

        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
            retConnection = 0; //CONNECTED
        }
    }

    /**
     * Checks if user is in the database and if password matches his username and is active
     * @param givenUsername - Entered username in the textfield
     * @param givenPassword - Entered password in the textfield
     * @return -1 if user NOT FOUND, otherwise his ID
     * @throws SQLException
     */
    public int checkUser(String givenUsername, String givenPassword) throws SQLException {
        LOGGER.info("<<< Checking if USER: >>>"+ givenUsername +"<<< exists and given password is correct and is active.");
        PreparedStatement selectUser = null;
        username = givenUsername;
        userID = -1;
        String selectQuery =
                "SELECT * FROM PERSON p INNER JOIN LOGIN l on p.loginid = l.loginid WHERE username = '" + givenUsername + "'AND password = '" + cryptWithMD5(givenPassword) + "' AND active = '1'";

        selectUser = c.prepareStatement(selectQuery);
        ResultSet result = selectUser.executeQuery();

        //If the user is found, return his ID as integer
        if(result.next()){
            userID = result.getInt(9);
            //CLEAN UP
            result.close();
            selectUser.close();
            LOGGER.info("<<< USER found.");
            return userID;
        }

        LOGGER.info("<<< Unfortunately combination of this username and password does not work.");
        //Else return -1
        return -1;
    }

    /**
     * @return retConnection, which represents connection status
     */
    public int getRetConnection(){
        return retConnection;
    }
}

