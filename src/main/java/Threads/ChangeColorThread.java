package Threads;

import javafx.scene.control.Button;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import static Main.App.LOGS_FOLDER;


public class ChangeColorThread extends Thread{

    //region LOGGER + filehandler
    private final static Logger LOGGER = Logger.getLogger(ChangeColorThread.class.getName());
    private static FileHandler fileHandler;
    //endregion

    //VARIABLES
    private Button BTN;
    private boolean result;
    private String officeName;
    private String floorName;

    /**
     * Constructor
     * @param BTN - Button to modify
     * @param result - Access granted if FALSE, access denied if TRUE
     * @param officeName - Name of office which user tries to access to
     */
    public ChangeColorThread(Button BTN, boolean result, String officeName, String floorName){
        this.BTN = BTN;
        this.result = result;
        this.officeName = officeName;
        this.floorName = floorName;
    }

    /**
     * Initialization of logger
     */
    public static void LoggerInit(){
        try {
            // This block configure the logger with handler and formatter
            fileHandler = new FileHandler(LOGS_FOLDER + "ChangeColorThread.log", true);
            LOGGER.addHandler(fileHandler);
            SimpleFormatter formatter = new SimpleFormatter();
            fileHandler.setFormatter(formatter);
            LOGGER.setLevel(Level.INFO);
            LOGGER.info("<<< Logging of " + ChangeColorThread.class.getName()+ " initiated successfully");

        } catch (SecurityException e) {
            e.printStackTrace();
            LOGGER.log( Level.SEVERE, e.toString(), e); //Always logging exceptions
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.log( Level.SEVERE, e.toString(), e); //Always logging exceptions
        }
    }

    /**
     * Makes the button blink for 2 seconds based on passed result
     */
    @Override
    public void run(){
        if(result){
            BTN.setStyle("-fx-background-color: crimson");
            LOGGER.info("<<< User tried to access: " + officeName + " on " + floorName + " --- Result > Access DENIED");
        }
        else{
            BTN.setStyle("-fx-background-color: chartreuse");
            LOGGER.info("<<< User tried to access: " + officeName + " on " + floorName + " --- Result > Access GRANTED");
        }
        try {
            BTN.setDisable(true);
            ChangeColorThread.sleep(2000); //Sleep for 2 seconds then change color back to default
            BTN.setDisable(false);
        } catch (InterruptedException e) {
            e.printStackTrace();
            LOGGER.log( Level.SEVERE, e.toString(), e); //Always logging exceptions
        }
        BTN.setStyle(null); //Change color back to default
    }
}
