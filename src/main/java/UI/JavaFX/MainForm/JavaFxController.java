package UI.JavaFX.MainForm;

import DBModels.*;
import Database.Db;
import Threads.ChangeColorThread;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.util.Callback;
import javafx.util.converter.DateTimeStringConverter;

import javax.persistence.Query;
import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import static Database.CryptWithMD5.cryptWithMD5;
import static Database.Db.getUsername;
import static Main.App.LOGS_FOLDER;
import static UI.JavaFX.MainForm.JavaFxRun.em;
import static UI.JavaFX.MainForm.JavaFxRun.emf;

public class JavaFxController {

    //region LOGGER + filehandler
    private final static Logger LOGGER = Logger.getLogger(JavaFxController.class.getName());
    private static FileHandler fileHandler;
    public String username = getUsername();
    //endregion
    //public String username = getUsername(); //Logged username
    String regex_only_letters= "^[,\\ \\.\\p{L}]*$+";
    String regex_only_numbers = "[0-9]+";
    String regex_email = "\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b";
    String regex_update = "(0|1|2)\\d:[0-5]\\d-(0|1|2)\\d:[0-5]\\d";
    //region TIME
    Calendar cal;
    String currentTime;
    String currentDay;
    //endregion
    String currentDate;
    //region QUERY
    Query query, query2;
    List<AccessLogEntity> getAccessIDs;
    List<Object> ListOfAccessObjects;
    //endregion
    Object currentAccessObject;
    private int datePickSet = 1; //Flag if date was set in testAccessTab
    private int userID = Db.getLoggedUserID(); //Logged LoginID
    private int personID = Db.getPersonID(userID); //Logged PersonID
    private SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
    private Object up_obj_member;
    private PersonEntity up_person;
    private CardEntity up_card;
    private AccessEntity up_1st, up_2nd, up_3rd, up_4th, up_5th, up_6th, up_7th;
    private int up_idmon, up_idtue, up_idwed, up_idthu, up_idfri, up_idsat, up_idsun;

    //region JAVA FX COMPONENTS

    @FXML
    private TableColumn<PersonEntity, String> upFirstName;
    @FXML
    private TableColumn<PersonEntity, String> upSurname;
    @FXML
    private TableColumn<PersonEntity, String> upEmployeeID;
    @FXML
    private TableColumn<PersonEntity, String> upActive;
    @FXML
    private Pane adminUsers;
    @FXML
    private TextField tf_upEmployeeID;
    @FXML
    private TableView<PersonEntity> tableUpdate;
    @FXML
    private Button button_adminUserDeactivate;
    @FXML
    private Button button_adminUserDelete;
    @FXML
    private TabPane tab_pane;
    @FXML
    private Label label_Hello;
    @FXML
    private Pane paneF1;
    @FXML
    private Pane paneF2;
    @FXML
    private Pane paneF3;
    @FXML
    private Pane paneF4;
    @FXML
    private Button show1;
    @FXML
    private Button show2;
    @FXML
    private Button button_adminPicture;
    @FXML
    private Button button_adminPicture2;
    @FXML
    private Tab accessLogsTab;

    //region DOORBUTTONS
    @FXML
    private Button Floor1_1_Office1_1_BTN;
    @FXML
    private Button Floor1_2_Office2_0_BTN;
    @FXML
    private Button Floor1_5_Office5_0_BTN;
    @FXML
    private Button Floor1_3_Office3_2_BTN;
    @FXML
    private Button Floor1_6_ConferenceRoom1_0_BTN;
    @FXML
    private Button Floor1_4_Office4_1_BTN;
    @FXML
    private Button Floor1_7_BreakRoom_2_BTN;
    @FXML
    private Button Floor1_15_Entry_2_BTN;
    @FXML
    private Button Floor2_8_Office21_0_BTN;
    @FXML
    private Button Floor2_9_Office22_2_BTN;
    @FXML
    private Button Floor2_13_ConferenceRoom21_0_BTN;
    @FXML
    private Button Floor2_10_Office23_1_BTN;
    @FXML
    private Button Floor2_12_Office25_0_BTN;
    @FXML
    private Button Floor2_14_Office26_0_BTN;
    @FXML
    private Button Floor2_11_Office24_2_BTN;
    @FXML
    private Button Floor1_1_Office1_1_BTN2;
    @FXML
    private Button Floor1_2_Office2_0_BTN2;
    @FXML
    private Button Floor1_5_Office5_0_BTN2;
    @FXML
    private Button Floor1_3_Office3_2_BTN2;
    @FXML
    private Button Floor1_6_ConferenceRoom1_0_BTN2;
    @FXML
    private Button Floor1_4_Office4_1_BTN2;
    @FXML
    private Button Floor1_7_BreakRoom_2_BTN2;
    @FXML
    private Button Floor1_15_Entry_2_BTN2;
    @FXML
    private Button Floor2_8_Office21_0_BTN2;
    @FXML
    private Button Floor2_9_Office22_2_BTN2;
    @FXML
    private Button Floor2_13_ConferenceRoom21_0_BTN2;
    @FXML
    private Button Floor2_10_Office23_1_BTN2;
    @FXML
    private Button Floor2_12_Office25_0_BTN2;
    @FXML
    private Button Floor2_14_Office26_0_BTN2;
    @FXML
    private Button Floor2_11_Office24_2_BTN2;
    //region TableAccesLogs
    @FXML
    private TableView<AccessLogEntity> tableLogs;
    @FXML
    private TableColumn<AccessLogEntity, String> accessDay;
    @FXML
    private TableColumn<AccessLogEntity, String> accessDayName;
    //endregion
    @FXML
    private TableColumn<AccessLogEntity, String> accessTime;
    @FXML
    private TableColumn<AccessLogEntity, Boolean> accessResult;
    @FXML
    private TableColumn<AccessLogEntity, String> accessOffice;
    @FXML
    private TextField timeSelector;
    @FXML
    private DatePicker dateSelector;
    @FXML
    private Tab tab_admin;
    //endregion

    @FXML
    private Tab liveAccessTab;
    @FXML
    private Tab testAccessTab;
    @FXML
    private Pane adminLogs;
    @FXML
    private Pane adminAddUser;

    //region UPDATE USER
    @FXML
    private Pane update_pane;
    @FXML
    private TextField update_accessmon;
    @FXML
    private TextField update_accesstue;
    @FXML
    private TextField update_accesswed;
    @FXML
    private TextField update_accessthu;
    @FXML
    private TextField update_accessfri;
    @FXML
    private TextField update_accesssat;
    @FXML
    private TextField update_accesssun;
    @FXML
    private Label update_username;
    @FXML
    private TextField update_cardnum;
    @FXML
    private TextField update_lvlmon;
    @FXML
    private TextField update_lvltue;
    @FXML
    private TextField update_lvlwed;
    @FXML
    private TextField update_lvlthu;
    @FXML
    private TextField update_lvlfri;
    @FXML
    private TextField update_lvlsat;
    @FXML
    private TextField update_lvlsun;
    @FXML
    private CheckBox update_checkmon;

    @FXML
    private CheckBox update_checktue;

    @FXML
    private CheckBox update_checkwed;

    @FXML
    private CheckBox update_checkthu;

    @FXML
    private CheckBox update_checkfri;

    @FXML
    private CheckBox update_checksat;

    @FXML
    private CheckBox update_checksun;
    //region ADD USER
    @FXML
    private TextField AU_firstname;
    @FXML
    private TextField AU_surname;
    @FXML
    private TextField AU_personalid;
    @FXML
    private TextField AU_email;
    @FXML
    private TextField AU_employeeid;
    @FXML
    private TextField AU_phonenum;
    @FXML
    private TextField AU_cardnum;
    //endregion
    @FXML
    private TextField AU_username;
    @FXML
    private PasswordField AU_password;
    @FXML
    private TextField AU_city;
    @FXML
    private TextField AU_housenum;
    @FXML
    private TextField AU_street;
    @FXML
    private TextField AU_zipcode;
    @FXML
    private CheckBox AU_day_mon;
    @FXML
    private CheckBox AU_day_tue;
    @FXML
    private CheckBox AU_day_wed;
    @FXML
    private CheckBox AU_day_thu;
    @FXML
    private CheckBox AU_day_fri;
    @FXML
    private CheckBox AU_day_sat;
    @FXML
    private CheckBox AU_day_sun;
    @FXML
    private RadioButton AU_male;
    @FXML
    private RadioButton AU_female;
    @FXML
    private TextField mon_from;
    @FXML
    private TextField mon_to;
    @FXML
    private TextField tue_from;
    @FXML
    private TextField tue_to;
    @FXML
    private TextField wed_from;
    @FXML
    private TextField wed_to;
    @FXML
    private TextField thu_from;
    @FXML
    private TextField thu_to;
    @FXML
    private TextField fri_from;
    @FXML
    private TextField fri_to;
    @FXML
    private TextField sat_from;
    @FXML
    private TextField sat_to;
    @FXML
    private TextField sun_from;
    @FXML
    private TextField sun_to;
    @FXML
    private TextField AU_acceslevel;
    @FXML
    private TableView<AccessLogEntity> tableLogsAdministrator;
    @FXML
    private TableColumn<AccessLogEntity, String> accessPersonAdmin;
    @FXML
    private TableColumn<AccessLogEntity, Date> accessDayAdmin;
    @FXML
    private TableColumn<AccessLogEntity, String> accessTimeAdmin;
    @FXML
    private TableColumn<AccessLogEntity, String> accessOfficeAdmin;
    @FXML
    private TableColumn<AccessLogEntity, String> accessFloorAdmin;
    @FXML
    private TableColumn<AccessLogEntity, Boolean> accessResultAdmin;
    //endregion
    @FXML
    private TableView<JoinpersontoaccessEntity> tableAccesses;
    @FXML
    private TableColumn<JoinpersontoaccessEntity, String> accessTimeFromTo;
    @FXML
    private TableColumn<JoinpersontoaccessEntity, String> accessLevel;
    @FXML
    private TableColumn<JoinpersontoaccessEntity, String> accessDayAllowed;
    @FXML
    private Tab my_access;

    @FXML
    void update_checkfri_f(ActionEvent event) {
        if(!update_checkfri.isSelected()) {
            update_accessfri.setDisable(true);
            update_lvlfri.setDisable(true);
        }
        else {
            update_accessfri.setDisable(false);
            update_lvlfri.setDisable(false);
        }
    }

    @FXML
    void update_checkmon_f(ActionEvent event) {
        if(!update_checkmon.isSelected()) {
            update_accessmon.setDisable(true);
            update_lvlmon.setDisable(true);
        }
        else {
            update_accessmon.setDisable(false);
            update_lvlmon.setDisable(false);
        }
    }

    @FXML
    void update_checksat_f(ActionEvent event) {
        if(!update_checksat.isSelected()) {
            update_accesssat.setDisable(true);
            update_lvlsat.setDisable(true);
        }
        else {
            update_accesssat.setDisable(false);
            update_lvlsat.setDisable(false);
        }
    }

    @FXML
    void update_checksun_f(ActionEvent event) {
        if(!update_checksun.isSelected()) {
            update_accesssun.setDisable(true);
            update_lvlsun.setDisable(true);
        }
        else {
            update_accesssun.setDisable(false);
            update_lvlsun.setDisable(false);
        }
    }

    @FXML
    void update_checkthu_f(ActionEvent event) {
        if(!update_checkthu.isSelected()) {
            update_accessthu.setDisable(true);
            update_lvlthu.setDisable(true);
        }
        else {
            update_accessthu.setDisable(false);
            update_lvlthu.setDisable(false);
        }
    }

    @FXML
    void update_checktue_f(ActionEvent event) {
        if(!update_checktue.isSelected()) {
            update_accesstue.setDisable(true);
            update_lvltue.setDisable(true);
        }
        else {
            update_accesstue.setDisable(false);
            update_lvltue.setDisable(false);
        }
    }

    @FXML
    void update_checkwed_f(ActionEvent event) {
        if(!update_checkwed.isSelected()) {
            update_accesswed.setDisable(true);
            update_lvlwed.setDisable(true);
        }
        else {
            update_accesswed.setDisable(false);
            update_lvlwed.setDisable(false);
        }
    }

    //endregion

    /**
     * Set user activity to 0
     * @param event - click on button deactivate
     */
    @FXML
    void adminDeactivateUser(ActionEvent event) {
        Object obj_user;
        PersonEntity person;
        obj_user = tableUpdate.getSelectionModel().getSelectedItem();
        person = (PersonEntity) obj_user;

        if (person.getActive().equals("0")) {
            em.clear();
            em.getTransaction().begin();
            person.setActive("1");
            em.merge(person);
            em.getTransaction().commit();
            em.clear();
            tableUpdate.refresh();
        }
        else {
            em.clear();
            em.getTransaction().begin();
            person.setActive("0");
            em.merge(person);
            em.getTransaction().commit();
            em.clear();
            tableUpdate.refresh();
        }
        tableUpdate.refresh();
    }

    /**
     * Delete user from database
     * @param event - click on button delete
     */
    @FXML
    void adminDeleteUser(ActionEvent event) {
        Object obj_member;
        PersonEntity person;
        LoginEntity login;
        CardEntity card;
        PersonAddressEntity address;

        obj_member = tableUpdate.getSelectionModel().getSelectedItem();
        person = (PersonEntity) obj_member;

        login = em.getReference(LoginEntity.class, person.getLoginid());
        card = em.getReference(CardEntity.class, person.getCardid());
        //address = em.getReference(PersonAddressEntity.class, person.getPersonAddressid());

        if (person.getActive().equals("0")) {
            //delete user
            LOGGER.info("<<< DELETING PERSON");
            em.clear();
            em.getTransaction().begin();
            em.remove(em.merge(person));
            LOGGER.info("Person entity has been removed.");
//            em.remove(login); //NOT NEEDED BECAUSE WE HAVE THE TRIGGER
            LOGGER.info("Person login has been removed");
//            em.remove(card); //NOT NEEDED BEACUSE WE HAVE THE TRIGGER
            LOGGER.info("Person card has been removed");
            /*em.remove(address);
            LOGGER.info("Person address has been removed");
            **/em.flush();
            em.getTransaction().commit();
            em.clear();

            /*em.getTransaction().begin();
            em.remove(PersonAddressEntity.class);
            em.flush();
            em.getTransaction().commit();
            em.clear();*/

            tableUpdate.getItems().remove(obj_member);
            tableUpdate.refresh();
        }
        else {
            showAlertError("You cannot delete active user!");
        }
    }

    /**
     * Check which day is going to be updated
     * @param day - day to update
     * @param accesstime - access time from and to that u want to update
     * @param lvl - lvl you want to update
     * @param  - object that is used to keep referency
     */
    void CheckDayForUpdate(String day, String accesstime, String lvl, int id) {
        if (day.equals("Mon")) {
            update_accessmon.setText(accesstime);
            update_lvlmon.setText(lvl);
            update_checkmon.setSelected(true);
            update_accessmon.setDisable(false);
            update_lvlmon.setDisable(false);
            up_idmon = id;
        } else if (day.equals("Tue")) {
            update_accesstue.setText(accesstime);
            update_lvltue.setText(lvl);
            update_checktue.setSelected(true);
            update_accesstue.setDisable(false);
            update_lvltue.setDisable(false);
            up_idtue = id;
        } else if (day.equals("Wed")) {
            update_accesswed.setText(accesstime);
            update_lvlwed.setText(lvl);
            update_checkwed.setSelected(true);
            update_accesswed.setDisable(false);
            update_lvlwed.setDisable(false);
            up_idwed = id;
        } else if (day.equals("Thu")) {
            update_accessthu.setText(accesstime);
            update_lvlthu.setText(lvl);
            update_checkthu.setSelected(true);
            update_accessthu.setDisable(false);
            update_lvlthu.setDisable(false);
            up_idthu = id;
        } else if (day.equals("Fri")) {
            update_accessfri.setText(accesstime);
            update_lvlfri.setText(lvl);
            update_checkfri.setSelected(true);
            update_accessfri.setDisable(false);
            update_lvlfri.setDisable(false);
            up_idfri = id;
        } else if (day.equals("Sat")) {
            update_accesssat.setText(accesstime);
            update_lvlsat.setText(lvl);
            update_checksat.setSelected(true);
            update_accesssat.setDisable(false);
            update_lvlsat.setDisable(false);
            up_idsat = id;
        } else {
            update_accesssun.setText(accesstime);
            update_lvlsun.setText(lvl);
            update_checksun.setSelected(true);
            update_accesssun.setDisable(false);
            update_lvlsun.setDisable(false);
            up_idsun = id;
        }
    }

    /**
     * Function that is called when you click on update user
     * @param event - click on update user
     */
    @FXML
    void adminUpdateUser(ActionEvent event) {

        em.clear();
        String id1, id2, id3, id4, id5, id6, id7;
        int id_1, id_2, id_3, id_4, id_5, id_6, id_7;
        up_obj_member = tableUpdate.getSelectionModel().getSelectedItem();
        up_person = (PersonEntity) up_obj_member;

        int CardId = up_person.getCardid();
        up_card = em.getReference(CardEntity.class, CardId);
        update_cardnum.setText(up_card.getCardnumber());

        int personid = up_person.getPersonid();

        query = em.createQuery("select accessid from JoinpersontoaccessEntity where personid =" + personid); //Selects all accessids for this user
        getAccessIDs = query.getResultList();

        try {
            //fill the TextFields with attributes of AccessEntity
            for(int i = 0; i < getAccessIDs.size(); i++){
                Object o1 = getAccessIDs.get(i);
                id1 = o1.toString();
                id_1 = Integer.valueOf(id1);
                up_1st = em.getReference(AccessEntity.class, id_1);
                String day = up_1st.getAccessdayallowed();
                CheckDayForUpdate(day, up_1st.getAccesstimefromto(), up_1st.getAccesslevel(), id_1);
            }
        } catch (RuntimeException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
        }


        adminUsers.setVisible(false);
        update_pane.setVisible(true);

    }

    /**
     * When u want to go back from updating a person
     * @param event - click on storno button
     */
    @FXML
    void update_storno(ActionEvent event) {
        update_pane.setVisible(false);
        adminUsers.setVisible(true);

        update_accessmon.clear();
        update_accesstue.clear();
        update_accesswed.clear();
        update_accessthu.clear();
        update_accessfri.clear();
        update_accesssat.clear();
        update_accesssun.clear();

        update_lvlmon.clear();
        update_lvltue.clear();
        update_lvlwed.clear();
        update_lvlthu.clear();
        update_lvlfri.clear();
        update_lvlsat.clear();
        update_lvlsun.clear();
    }

    /**
     * Check if this Access exist
     * @param accesslvl - access level
     * @param day - day with access
     * @param timefromto - time from and to
     * @return -1 if access is not found, else return access id
     */
    private int up_checkAccessIfExists (String accesslvl, String day, String timefromto) {
        int AccessID = -1;
        Db database = new Db();
        database.openConnection();
        if(database.getRetConnection()==0) {
            try {
                AccessID = Db.getAccessID(accesslvl, day, timefromto);
            }
            finally {
                database.closeConnection(); //Closing the connection to db
            }
        }
        return AccessID;
    }

    /**
     * Check person's access
     * @param PersonId - id of a person
     * @param AccessId - id of an access
     * @return -1 if not found, else return id of joinpersontoacceess
     */
    private int checkJpta (int PersonId, int AccessId) {
        int JptaID = -1;
        Db database = new Db();
        database.openConnection();
        if(database.getRetConnection()==0) {
            try {
                JptaID = Db.getJptaID(AccessId, PersonId);
            }
            finally {
                database.closeConnection(); //Closing the connection to db
            }
        }
        return JptaID;
    }

    /**
     * Update values from form and add them to databese
     * @param event - click on update user
     */
    @FXML
    void update_user(ActionEvent event) {
        //if checkbox is selected, then update data in database
        int PersonID = up_person.getPersonid();

        //region Validation
        if (update_cardnum.getText().matches(regex_only_numbers) && update_cardnum.getLength() == 8) {

        }
        else {
            showAlertError("Card does not match required pattern: 8 numbers");
            return;
        }
        //endregion

        //region Monday
        if (update_checkmon.isSelected()) {
            if (update_accessmon.getText().matches(regex_update)) {
                if (update_lvlmon.getText().equals("0") || update_lvlmon.getText().equals("1") || update_lvlmon.getText().equals("2")) {

                }
                else {
                    showAlertError("Access time monday is not in valid format");
                    return;
                }
            }
            else {
                showAlertError("Monday access time does not match required pattern: HH:mm-HH:mm");
                return;
            }
            //check if access already exists
            AccessEntity up_mon = em.getReference(AccessEntity.class, up_idmon);
            int AccessID = up_checkAccessIfExists(update_lvlmon.getText(), "Mon", update_accessmon.getText());

            //delete old association
            em.clear();
            int JptaIDold = checkJpta(PersonID, up_idmon);

            if (JptaIDold>0) {
                //if user had access this day, delete old
                JoinpersontoaccessEntity JptaEntityOld;
                JptaEntityOld = em.getReference(JoinpersontoaccessEntity.class, JptaIDold);
                em.getTransaction().begin();
                em.remove(em.merge(JptaEntityOld));
                em.getTransaction().commit();
                em.clear();
            }

            if (AccessID>0) {
                //access exist
                //use selected access and join it to a person
                insertToJoinPersonToAccess(PersonID, AccessID);

            }
            else {
                //access did not exist, create new access
                //use new access and join it to a person
                AccessEntity EntityAccess = new AccessEntity();
                EntityAccess.setAccesslevel(update_lvlmon.getText());
                EntityAccess.setAccessdayallowed("Mon");
                EntityAccess.setAccesstimefromto(update_accessmon.getText());
                EntityManagerStuff(EntityAccess);
                int dayID = EntityAccess.getAccessid();

                insertToJoinPersonToAccess(PersonID, dayID);

            }
        }
        //endregion

        //region Tuesday
        if (update_checktue.isSelected()) {
            if (update_accesstue.getText().matches(regex_update)) {
                if (update_lvltue.getText().equals("0") || update_lvltue.getText().equals("1") || update_lvltue.getText().equals("2")) {

                }
                else {
                    showAlertError("Access time is not in valid format");
                    return;
                }
            }
            else {
                showAlertError("Tuesday access time does not match required pattern: HH:mm-HH:mm");
                return;
            }
            //check if access already exists
            AccessEntity up_tue = em.getReference(AccessEntity.class, up_idtue);
            int AccessID = up_checkAccessIfExists(update_lvltue.getText(), "Tue", update_accesstue.getText());

            //delete old association
            em.clear();
            int JptaIDold = checkJpta(PersonID, up_idtue);

            if (JptaIDold>0) {
                //if user had access this day, delete old
                JoinpersontoaccessEntity JptaEntityOld;
                JptaEntityOld = em.getReference(JoinpersontoaccessEntity.class, JptaIDold);
                em.getTransaction().begin();
                em.remove(em.merge(JptaEntityOld));
                em.getTransaction().commit();
                em.clear();
            }

            if (AccessID>0) {
                //access exist
                //use selected access and join it to a person
                insertToJoinPersonToAccess(PersonID, AccessID);

            }
            else {
                //access did not exist, create new access
                //use new access and join it to a person
                AccessEntity EntityAccess = new AccessEntity();
                EntityAccess.setAccesslevel(update_lvltue.getText());
                EntityAccess.setAccessdayallowed("Tue");
                EntityAccess.setAccesstimefromto(update_accesstue.getText());
                EntityManagerStuff(EntityAccess);
                int dayID = EntityAccess.getAccessid();

                insertToJoinPersonToAccess(PersonID, dayID);

            }
        }
        //endregion

        //region Wednesday
        if (update_checkwed.isSelected()) {
            if (update_accesswed.getText().matches(regex_update)) {
                if (update_lvlwed.getText().equals("0") || update_lvlwed.getText().equals("1") || update_lvlwed.getText().equals("2")) {

                }
                else {
                    showAlertError("Access time is not in valid format");
                    return;
                }
            }
            else {
                showAlertError("Wednesday access time does not match required pattern: HH:mm-HH:mm");
                return;
            }
            //check if access already exists
            AccessEntity up_wed = em.getReference(AccessEntity.class, up_idwed);
            int AccessID = up_checkAccessIfExists(update_lvlwed.getText(), "Wed", update_accesswed.getText());

            //delete old association
            em.clear();
            int JptaIDold = checkJpta(PersonID, up_idwed);

            if (JptaIDold>0) {
                //if user had access this day, delete old
                JoinpersontoaccessEntity JptaEntityOld;
                JptaEntityOld = em.getReference(JoinpersontoaccessEntity.class, JptaIDold);
                em.getTransaction().begin();
                em.remove(em.merge(JptaEntityOld));
                em.getTransaction().commit();
                em.clear();
            }

            if (AccessID>0) {
                //access exist
                //use selected access and join it to a person
                insertToJoinPersonToAccess(PersonID, AccessID);

            }
            else {
                //access did not exist, create new access
                //use new access and join it to a person
                AccessEntity EntityAccess = new AccessEntity();
                EntityAccess.setAccesslevel(update_lvlwed.getText());
                EntityAccess.setAccessdayallowed("Wed");
                EntityAccess.setAccesstimefromto(update_accesswed.getText());
                EntityManagerStuff(EntityAccess);
                int dayID = EntityAccess.getAccessid();

                insertToJoinPersonToAccess(PersonID, dayID);

            }
        }
        //endregion

        //region Thursday
        if (update_checkthu.isSelected()) {
            if (update_accessthu.getText().matches(regex_update)) {
                if (update_lvlthu.getText().equals("0") || update_lvlthu.getText().equals("1") || update_lvlthu.getText().equals("2")) {

                }
                else {
                    showAlertError("Access time is not in valid format");
                    return;
                }
            }
            else {
                showAlertError("Thursday access time does not match required pattern: HH:mm-HH:mm");
                return;
            }
            //check if access already exists
            AccessEntity up_thu = em.getReference(AccessEntity.class, up_idthu);
            int AccessID = up_checkAccessIfExists(update_lvlthu.getText(), "Thu", update_accessthu.getText());

            //delete old association
            em.clear();
            int JptaIDold = checkJpta(PersonID, up_idthu);

            if (JptaIDold>0) {
                //if user had access this day, delete old
                JoinpersontoaccessEntity JptaEntityOld;
                JptaEntityOld = em.getReference(JoinpersontoaccessEntity.class, JptaIDold);
                em.getTransaction().begin();
                em.remove(em.merge(JptaEntityOld));
                em.getTransaction().commit();
                em.clear();
            }

            if (AccessID>0) {
                //access exist
                //use selected access and join it to a person
                insertToJoinPersonToAccess(PersonID, AccessID);

            }
            else {
                //access did not exist, create new access
                //use new access and join it to a person
                AccessEntity EntityAccess = new AccessEntity();
                EntityAccess.setAccesslevel(update_lvlthu.getText());
                EntityAccess.setAccessdayallowed("Thu");
                EntityAccess.setAccesstimefromto(update_accessthu.getText());
                EntityManagerStuff(EntityAccess);
                int dayID = EntityAccess.getAccessid();

                insertToJoinPersonToAccess(PersonID, dayID);

            }
        }
        //endregion

        //region Friday
        if (update_checkfri.isSelected()) {
            if (update_accessfri.getText().matches(regex_update)) {
                if (update_lvlfri.getText().equals("0") || update_lvlfri.getText().equals("1") || update_lvlfri.getText().equals("2")) {

                }
                else {
                    showAlertError("Access time is not in valid format");
                    return;
                }
            }
            else {
                showAlertError("Friday access time does not match required pattern: HH:mm-HH:mm");
                return;
            }
            //check if access already exists
            AccessEntity up_fri = em.getReference(AccessEntity.class, up_idfri);
            int AccessID = up_checkAccessIfExists(update_lvlfri.getText(), "Fri", update_accessfri.getText());

            //delete old association
            em.clear();
            int JptaIDold = checkJpta(PersonID, up_idfri);

            if (JptaIDold>0) {
                //if user had access this day, delete old
                JoinpersontoaccessEntity JptaEntityOld;
                JptaEntityOld = em.getReference(JoinpersontoaccessEntity.class, JptaIDold);
                em.getTransaction().begin();
                em.remove(em.merge(JptaEntityOld));
                em.getTransaction().commit();
                em.clear();
            }

            if (AccessID>0) {
                //access exist
                //use selected access and join it to a person
                insertToJoinPersonToAccess(PersonID, AccessID);

            }
            else {
                //access did not exist, create new access
                //use new access and join it to a person
                AccessEntity EntityAccess = new AccessEntity();
                EntityAccess.setAccesslevel(update_lvlfri.getText());
                EntityAccess.setAccessdayallowed("Fri");
                EntityAccess.setAccesstimefromto(update_accessfri.getText());
                EntityManagerStuff(EntityAccess);
                int dayID = EntityAccess.getAccessid();

                insertToJoinPersonToAccess(PersonID, dayID);

            }
        }
        //endregion

        //region Saturday
        if (update_checksat.isSelected()) {
            if (update_accesssat.getText().matches(regex_update)) {
                if (update_lvlsat.getText().equals("0") || update_lvlsat.getText().equals("1") || update_lvlsat.getText().equals("2")) {

                }
                else {
                    showAlertError("Access time is not in valid format");
                    return;
                }
            }
            else {
                showAlertError("Saturday access time does not match required pattern: HH:mm-HH:mm");
                return;
            }
            //check if access already exists
            AccessEntity up_sat = em.getReference(AccessEntity.class, up_idsat);
            int AccessID = up_checkAccessIfExists(update_lvlsat.getText(), "Sat", update_accesssat.getText());

            //delete old association
            em.clear();
            int JptaIDold = checkJpta(PersonID, up_idsat);

            if (JptaIDold>0) {
                //if user had access this day, delete old
                JoinpersontoaccessEntity JptaEntityOld;
                JptaEntityOld = em.getReference(JoinpersontoaccessEntity.class, JptaIDold);
                em.getTransaction().begin();
                em.remove(em.merge(JptaEntityOld));
                em.getTransaction().commit();
                em.clear();
            }

            if (AccessID>0) {
                //access exist
                //use selected access and join it to a person
                insertToJoinPersonToAccess(PersonID, AccessID);

            }
            else {
                //access did not exist, create new access
                //use new access and join it to a person
                AccessEntity EntityAccess = new AccessEntity();
                EntityAccess.setAccesslevel(update_lvlsat.getText());
                EntityAccess.setAccessdayallowed("Sat");
                EntityAccess.setAccesstimefromto(update_accesssat.getText());
                EntityManagerStuff(EntityAccess);
                int dayID = EntityAccess.getAccessid();

                insertToJoinPersonToAccess(PersonID, dayID);

            }
        }
        //endregion

        //region Sunday
        if (update_checksun.isSelected()) {
            if (update_accesssun.getText().matches(regex_update)) {
                if (update_lvlsun.getText().equals("0") || update_lvlsun.getText().equals("1") || update_lvlsun.getText().equals("2")) {

                }
                else {
                    showAlertError("Access time is not in valid format");
                    return;
                }
            }
            else {
                showAlertError("Sunday access time does not match required pattern: HH:mm-HH:mm");
                return;
            }
            //check if access already exists
            AccessEntity up_sun = em.getReference(AccessEntity.class, up_idsun);
            int AccessID = up_checkAccessIfExists(update_lvlsun.getText(), "Sun", update_accesssun.getText());

            //delete old association
            em.clear();
            int JptaIDold = checkJpta(PersonID, up_idsun);

            if (JptaIDold>0) {
                //if user had access this day, delete old
                JoinpersontoaccessEntity JptaEntityOld;
                JptaEntityOld = em.getReference(JoinpersontoaccessEntity.class, JptaIDold);
                em.getTransaction().begin();
                em.remove(em.merge(JptaEntityOld));
                em.getTransaction().commit();
                em.clear();
            }

            if (AccessID>0) {
                //access exist
                //use selected access and join it to a person
                insertToJoinPersonToAccess(PersonID, AccessID);

            }
            else {
                //access did not exist, create new access
                //use new access and join it to a person
                AccessEntity EntityAccess = new AccessEntity();
                EntityAccess.setAccesslevel(update_lvlsun.getText());
                EntityAccess.setAccessdayallowed("Sun");
                EntityAccess.setAccesstimefromto(update_accesssun.getText());
                EntityManagerStuff(EntityAccess);
                int dayID = EntityAccess.getAccessid();

                insertToJoinPersonToAccess(PersonID, dayID);

            }
        }
        //endregion

        int CardID = checkCardIfExists(update_cardnum.getText());

        if (CardID<0) {
            //set new card
            em.clear();
            em.getTransaction().begin();
            up_card.setCardnumber(update_cardnum.getText());
            em.merge(up_card);
            em.getTransaction().commit();
            em.clear();
        }
        else {
            //card is same or already exists
            //for now, nothing to do
        }

        update_pane.setVisible(false);
        adminUsers.setVisible(true);

    }

    /**
     * Making first floor visible
     * @param event - clicking the button
     */
    @FXML
    void setVisible1(ActionEvent event) {
        paneF1.setVisible(true);
        paneF2.setVisible(false);
        paneF3.setVisible(true);
        paneF4.setVisible(false);
    }

    /**
     * Making second floor visible
     * @param event - clicking the button
     */
    @FXML
    void setVisible2(ActionEvent event) {
        paneF1.setVisible(false);
        paneF2.setVisible(true);
        paneF3.setVisible(false);
        paneF4.setVisible(true);
    }


    /**
     * Inserts new row into access_log table
     * @param CurrentDate - actual date
     * @param Time - actual time
     * @param result - result of the access attempt WARNING: False = ACCESS GRANTED, True = ACCESS DENIED
     * @param officeRoomID - ID of accessed office
     */
    void insertAccessLog(String CurrentDate, String Time, boolean result, int officeRoomID){

        AccessLogEntity newAccessLog = new AccessLogEntity();
        newAccessLog.setAccessday(CurrentDate);
        newAccessLog.setAccesstime(Time);
        newAccessLog.setAccessresult(result);
        newAccessLog.setOfficeroomid(officeRoomID);
        newAccessLog.setPersonid(personID);
        newAccessLog.setAccessdayname(CurrentDate);
        em.getTransaction().begin();
        em.persist(newAccessLog);
        em.flush();
        em.getTransaction().commit();
        em.clear();

        //LOGGING
        LOGGER.info("<<< New access log added. User was accessing via live access.\n Time: " + currentTime + "\n Date: " + currentDate +"\n Result: "+result);
    }


    /**
     * Method saves current date and time, selects all accesses of currently signed user and checks whether the user can access the office or not
     * @param levelRequired - required level of the office
     * @return - !!! result of the access attempt WARNING: False = ACCESS GRANTED, True = ACCESS DENIED !!!
     */
    private boolean accessCheck(int levelRequired){
        /*
        * TIME ENTRIES - will be inserted into access_log table
        * currentTime >> example: 22:54
        * currentDay >> example: "Sat"
        * currentDate >> example: "13.05.17"
        * */
        em.clear();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yy");
        Date date;
        SimpleDateFormat dateFormat;
        Alert alert;
        if(liveAccessTab.isSelected()){
            cal = Calendar.getInstance();
            currentTime = sdf.format(cal.getTime());

            date = cal.getTime();
            currentDay = new SimpleDateFormat("EE", Locale.ENGLISH).format(date.getTime());

            LocalDate localDate = LocalDate.now();
            currentDate = dtf.format(localDate);
        }

        else if(testAccessTab.isSelected()){

            if(dateSelector.getValue() == null){
                alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("You have to set the date!");
                alert.showAndWait();

                LOGGER.warning("### User did not pick time, function aborted.");
                datePickSet = -1; //DATEPICK WAS NOT SET
                return true;
            }
            datePickSet = 1; //DATEPICK WAS SET

            try {
                currentTime = timeSelector.getText();
                currentDate = dtf.format(dateSelector.getValue());
                date = new SimpleDateFormat("dd.MM.yy").parse(currentDate);
                currentDay = new SimpleDateFormat("EE", Locale.ENGLISH).format(date);
            } catch (ParseException e) {
                e.printStackTrace();
                LOGGER.log( Level.SEVERE, e.toString(), e); //Always logging exceptions
            }
        }

        query = em.createQuery("select accessid from JoinpersontoaccessEntity where personid =" + personID); //Selects all accessids for this user
        getAccessIDs = query.getResultList();

        ListOfAccessObjects = new ArrayList<Object>(); //Objects of accesses

        int i;
        int listSize = getAccessIDs.size();
        //Cycle to get all of the user's accesses
        for(i = 0; i < listSize; i++){
            query = em.createQuery("from AccessEntity where accessid =" + getAccessIDs.get(i));
            currentAccessObject = query.getSingleResult();
            ListOfAccessObjects.add(currentAccessObject);
        }

        int ListOfAccessObjectsSize = ListOfAccessObjects.size();
        Object value; //Value stores current field value
        int countIfArrayFilled; //If this equals 3, then the Array is filled
        //Iterating through access objects
        for(i = 0; i < ListOfAccessObjectsSize; i++){
            countIfArrayFilled = 0;
            currentAccessObject = ListOfAccessObjects.get(i);

            /*
            * Get each field of the current Access Object in this order:
            * 1. ID
            * 2. Accessdayallowed - example "Mon"
            * 3. Accesstimeinfo - example "08:00-15:00"
            * 4. Accesslevel - example "0"
            * */
            String decidingValues[] = new String[3]; //Filling with accessdayallowed, accesstimeinfo, accesslevel
            for (Field field : currentAccessObject.getClass().getDeclaredFields()){
                field.setAccessible(true);
                value = null;
                try {
                    value = field.get(currentAccessObject);
                    if(field.getName().equals("accessdayallowed")){
                        decidingValues[0] = value.toString();
                        countIfArrayFilled++;
                    }
                    else if(field.getName().equals("accesstimefromto")){
                        decidingValues[2] = value.toString();
                        countIfArrayFilled++;
                    }
                    else if(field.getName().equals("accesslevel")){
                        decidingValues[1] = value.toString();
                        countIfArrayFilled++;
                    }

                    //We got all information and can go to CHECK
                    if(countIfArrayFilled == 3){
                        countIfArrayFilled = 0;

                        //DAY CHECK
                        if(decidingValues[0].equals(currentDay)){
                            /*
                            * LEVEL CHECK
                            * 0 is the highest access level, 2 is the lowest*/
                            if(Integer.parseInt(decidingValues[1]) <= levelRequired){
                                //Parse the time
                                String SplitTime[] = decidingValues[2].split("-");
                                String StartTimes = SplitTime[0];
                                String EndTimes = SplitTime[1];

                                try {
                                    Date compareFrom = sdf.parse(StartTimes);
                                    Date compareTo = sdf.parse(EndTimes);
                                    Date actTime = sdf.parse(currentTime);

                                    //CHECK TIMES
                                    if((compareFrom.before(actTime) || compareFrom.equals(actTime)) && (compareTo.after(actTime) || compareTo.equals(actTime))){
                                        return false; //UNLOCKED
                                    }

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                    LOGGER.log( Level.SEVERE, e.toString(), e); //Always logging exceptions
                                    return true; //LOCKED
                                }
                            }
                        }
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                    LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
                    return true; //LOCKED
                }
            }
        }
        return true;
    }

    /**
     * Select user's accesses and view them to tableView
     */
    @FXML
    void seeAccesses(){
        //If my_access tab is selected
        if(my_access.isSelected()){
            LOGGER.info("<<< User requires to see his accesses");
            query = em.createQuery("from JoinpersontoaccessEntity where personByPersonid.loginByLoginid.username='"+Db.username+"'");
            List<JoinpersontoaccessEntity> list = query.getResultList();
            ObservableList<JoinpersontoaccessEntity> proList = FXCollections.observableArrayList(list);

            accessDayAllowed.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JoinpersontoaccessEntity, String>, ObservableValue<String>>() {
                @Override
                public ObservableValue<String> call(TableColumn.CellDataFeatures<JoinpersontoaccessEntity, String> param) {
                    return new SimpleStringProperty(param.getValue().getAccessByAccessid().getAccessdayallowed());
                }
            });

            accessTimeFromTo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JoinpersontoaccessEntity, String>, ObservableValue<String>>() {
                @Override
                public ObservableValue<String> call(TableColumn.CellDataFeatures<JoinpersontoaccessEntity, String> param) {
                    return new SimpleStringProperty(param.getValue().getAccessByAccessid().getAccesstimefromto());
                }
            });

            accessLevel.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JoinpersontoaccessEntity, String>, ObservableValue<String>>() {
                @Override
                public ObservableValue<String> call(TableColumn.CellDataFeatures<JoinpersontoaccessEntity, String> param) {
                    return new SimpleStringProperty(param.getValue().getAccessByAccessid().getAccesslevel());
                }
            });

            tableAccesses.setItems(null);
            tableAccesses.setItems(proList);
        }
    }

    /**
     * Select access logs for currently signed user and display it to tableview
     */
    @FXML
    void seeAccessLogs(){
        //If access log tab is selected
        if(accessLogsTab.isSelected()){
            LOGGER.info("<<< User requires to see his access logs");
            query = em.createQuery("from AccessLogEntity where personByPersonid.loginByLoginid.username='"+Db.username+"'");
            List<AccessLogEntity> list = query.getResultList();
            ObservableList<AccessLogEntity> proList = FXCollections.observableArrayList(list);

            accessDay.setCellValueFactory(new PropertyValueFactory<AccessLogEntity, String>("accessday"));
            accessDayName.setCellValueFactory(new PropertyValueFactory<AccessLogEntity, String>("accessdayname"));
            accessTime.setCellValueFactory(new PropertyValueFactory<AccessLogEntity, String>("accesstime"));
            accessResult.setCellValueFactory(new PropertyValueFactory<AccessLogEntity, Boolean>("accessresult"));
            accessOffice.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<AccessLogEntity, String>, ObservableValue<String>>() {
                @Override
                public ObservableValue<String> call(TableColumn.CellDataFeatures<AccessLogEntity, String> param) {
                    return new SimpleStringProperty(param.getValue().getOfficeroomByOfficeroomid().getName());
                }
            });

            tableLogs.setItems(null);
            tableLogs.setItems(proList);
        }
    }

    /**
     * Functionality in form
     */
    //region Access_days
    @FXML
    void AU_day_fri_clicked(ActionEvent event) {
        if (AU_day_fri.isSelected()) {
            fri_from.setDisable(false);
            fri_to.setDisable(false);
        }
        else {fri_from.setDisable(true); fri_to.setDisable(true);}
    }

    @FXML
    void AU_day_mon_clicked(ActionEvent event) {
        if (AU_day_mon.isSelected()) {
            mon_from.setDisable(false);
            mon_to.setDisable(false);
        }
        else {mon_from.setDisable(true); mon_to.setDisable(true);}
    }

    @FXML
    void AU_day_sat_clicked(ActionEvent event) {
        if (AU_day_sat.isSelected()) {
            sat_from.setDisable(false);
            sat_to.setDisable(false);
        }
        else {sat_from.setDisable(true); sat_to.setDisable(true); }
    }

    @FXML
    void AU_day_sun_clicked(ActionEvent event) {
        if (AU_day_sun.isSelected()) {
            sun_from.setDisable(false);
            sun_to.setDisable(false);
        }
        else {sun_from.setDisable(true); sun_to.setDisable(true); }
    }

    @FXML
    void AU_day_thu_clicked(ActionEvent event) {
        if (AU_day_thu.isSelected()) {
            thu_from.setDisable(false);
            thu_to.setDisable(false);
        }
        else {thu_from.setDisable(true); thu_to.setDisable(true); }
    }

    @FXML
    void AU_day_tue_clicked(ActionEvent event) {
        if (AU_day_tue.isSelected()) {
            tue_from.setDisable(false);
            tue_to.setDisable(false);
        }
        else {tue_from.setDisable(true); tue_to.setDisable(true);}
    }

    @FXML
    void AU_day_wed_clicked(ActionEvent event) {
        if (AU_day_wed.isSelected()) {
            wed_from.setDisable(false);
            wed_to.setDisable(false);
        }
        else {wed_from.setDisable(true); wed_to.setDisable(true); }
    }
    //endregion

    /**
     * Function to work with entity manager
     * @param entity - object which i want to work with
     */
    void EntityManagerStuff(Object entity) {
        em.getTransaction().begin();
        em.persist(entity);
        em.flush();
        em.getTransaction().commit();
    }

    /**
     * Function to insert to AccessEntity and JoinpersontoaccessEntity
     * @param AccessLevel - persons access levels
     * @param day - day to access
     * @param DayFrom - time from which person can access
     * @param DayTo - time to which person can access
     * @param PID - person's id
     */
    void insertToAccess (String AccessLevel, String day, String DayFrom, String DayTo, int PID) {
        AccessEntity EntityAccess = new AccessEntity();
        EntityAccess.setAccesslevel(AccessLevel);
        EntityAccess.setAccessdayallowed(day);
        EntityAccess.setAccesstimefromto(DayFrom+"-"+DayTo);
        EntityManagerStuff(EntityAccess);
        int dayID = EntityAccess.getAccessid();

        JoinpersontoaccessEntity newJoinMon = new JoinpersontoaccessEntity();
        newJoinMon.setPersonid(PID);
        newJoinMon.setAccessid(dayID);
        EntityManagerStuff(newJoinMon);
    }

    /**
     * Function to insert to Joinpersontoaccess
     * @param PersonID - person's id
     * @param AccessID - id of access
     */
    void insertToJoinPersonToAccess (int PersonID, int AccessID) {
        JoinpersontoaccessEntity newJoinMon = new JoinpersontoaccessEntity();
        newJoinMon.setPersonid(PersonID);
        newJoinMon.setAccessid(AccessID);
        EntityManagerStuff(newJoinMon);
    }

    /**
     * Check if same address already exists
     * @param city - city
     * @param housenum - house number
     * @param zipcode - zip code
     * @param street - street
     * @return -1 if not found, else return address id
     */
    private int checkAddressIfExists (String city, String housenum, String zipcode, String street) {
        int AddrID = -1;
        Db database = new Db();
        database.openConnection();
        if(database.getRetConnection()==0) {
            try {
                AddrID = Db.getAddressID(city, housenum, zipcode, street);
                System.out.print(AddrID);
            }
            finally {
                database.closeConnection(); //Closing the connection to db
            }
        }
        return AddrID;
    }

    /**
     * Just calls checkAddressIfExists, for unit testing
     */
    public int checkAddressIfExistsCall(String city, String housenum, String zipcode, String street){
        return checkAddressIfExists(city, housenum, zipcode, street);
    }

    /**
     * Check if same access already exists
     * @param accesslvl - lvl of access
     * @param day - access day
     * @param timefrom - time which user can access
     * @param timeto - time to which can user access
     * @return -1 if not found, else return access id
     */
    private int checkAccessIfExists (String accesslvl, String day, String timefrom, String timeto) {
        String time = timefrom+"-"+timeto;
        int AccessID = -1;
        Db database = new Db();
        database.openConnection();
        if(database.getRetConnection()==0) {
            try {
                AccessID = Db.getAccessID(accesslvl, day, time);
            }
            finally {
                database.closeConnection(); //Closing the connection to db
            }
        }
        return AccessID;
    }

    /**
     * Check if same card number already exists
     * @param cardnum - number of a card
     * @return -1 if not found, else return card id
     */
    private int checkCardIfExists (String cardnum) {
        int CardID = -1;
        Db database = new Db();
        database.openConnection();
        if(database.getRetConnection()==0) {
            try {
                CardID = Db.getCardID(cardnum);
            }
            finally {
                database.closeConnection(); //Closing the connection to db
            }
        }
        return CardID;
    }

    /**
     * Just calling method for unit tests
     */
    public int checkCardIfExistsCall(String cardnum){
        return checkCardIfExists(cardnum);
    }

    /**
     * Check if same login name already exists
     * @param username - username of person
     * @return -1 if not found, else return username id
     */
    private int checkLoginIfExists (String username) {
        int LoginID = -1;
        Db database = new Db();
        database.openConnection();
        if(database.getRetConnection()==0) {
            try {
                LoginID = Db.getLoginID(username);
            }
            finally {
                database.closeConnection(); //Closing the connection to db
            }
        }
        return LoginID;
    }

    /**
     * Just calling method for unit tests
     */
    public int checkLoginIfExistsCall(String username){
        return checkLoginIfExists(username);
    }

    /**
     * Show errror window
     * @param text - text to show in window
     */
    void showAlertError (String text) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error Dialog");
        alert.setHeaderText(null);
        alert.setContentText(text);
        alert.showAndWait();
    }

    /**
     * Check if email exists in database
     * @param email - email of a person
     * @return -1 if not, else 1
     */
    private int checkEmailIfExists (String email) {
        int result = 1;
        Db database = new Db();
        database.openConnection();
        try {
            result = Db.checkEmailIfExists(email);
        }
        finally {
            database.closeConnection(); //Closing the connection to db
        }
        return result;
    }

    /**
     * Check if employee id exists in database
     * @param employeeid - employee id of a person
     * @return -1 if not found, else 1
     */
    private int checkEmployeeidIfExists (String employeeid) {
        int result = 1;
        Db database = new Db();
        database.openConnection();
        try {
            result = Db.checkEmployeeidIfExists(employeeid);
        }
        finally {
            database.closeConnection(); //Closing the connection to db
        }
        return result;
    }

    /**
     * Check if personal id exists in database
     * @param personalid - personal id of a person
     * @return -1 if not found, else 1
     */
    private int checkPersonalidIfExists (String personalid) {
        int result = 1;
        Db database = new Db();
        database.openConnection();
        try {
            result = Db.checkPersonalidIfExists(personalid);
        }
        finally {
            database.closeConnection(); //Closing the connection to db
        }
        return result;
    }

    /**
     * Just calling method for unit tests
     */
    public int checkPersonlidIfExistsCall(String personalid){
        return checkPersonalidIfExists(personalid);
    }

    /**
     * Check if phone number already exists in database
     * @param phonenumber - phone number of a person
     * @return -1 if not found, else 1
     */
    private int checkPhonenumberIfExists (String phonenumber) {
        int result = 1;
        Db database = new Db();
        database.openConnection();
        try {
            result = Db.checkPhonenumberIfExists(phonenumber);
        }
        finally {
            database.closeConnection(); //Closing the connection to db
        }
        return result;
    }

    /**
     * Function to add user to database
     * @param event - button add user was clicked
     */
    @FXML
    void addPersonToDatabase(ActionEvent event) {

        int PersonID;
        String sex = "";
        int AddrID = checkAddressIfExists(AU_city.getText(), AU_housenum.getText(), AU_zipcode.getText(), AU_street.getText());
        int CardID = checkCardIfExists(AU_cardnum.getText());
        int LoginID = checkLoginIfExists(AU_username.getText());
        int checkMail = checkEmailIfExists(AU_email.getText());
        int checkEmployeeid = checkEmployeeidIfExists(AU_employeeid.getText());
        int checkPersonalid = checkPersonalidIfExists(AU_personalid.getText());
        int checkPhonenumber = checkPhonenumberIfExists(AU_phonenum.getText());

        //region Person validation
        if (checkMail<0) {
            if (checkEmployeeid<0) {
                if (checkPersonalid<0) {
                    if (checkPhonenumber<0) {

                    }
                    else {
                        showAlertError("Phone number already exists");
                        return;
                    }
                }
                else {
                    showAlertError("PersonalID already exists");
                    return;
                }
            }
            else {
                showAlertError("EmployeeID already exists");
                return;
            }
        }
        else {
            showAlertError("Email already exists");
            return;
        }
        //endregion

        //region Other validation
        if (    (AU_cardnum.getLength() == 8 && AU_cardnum.getText().matches(regex_only_numbers) &&
                AU_username.getLength()<=20 && AU_password.getLength()<=20 &&
                AU_city.getLength()<=30 && AU_housenum.getLength()<=10 && AU_street.getLength()<=30 && AU_zipcode.getLength()<=10 && AU_housenum.getText().matches(regex_only_numbers) && AU_zipcode.getText().matches(regex_only_numbers) && AU_city.getText().matches(regex_only_letters) && AU_street.getText().matches(regex_only_letters) &&
                AU_email.getText().matches(regex_email) && AU_email.getLength()<=30 &&
                AU_employeeid.getText().matches(regex_only_numbers) && AU_employeeid.getLength()==8 &&
                AU_firstname.getText().matches(regex_only_letters) && AU_firstname.getLength()<=20 &&
                AU_surname.getText().matches(regex_only_letters) && AU_surname.getLength()<=20 &&
                AU_personalid.getText().matches(regex_only_numbers) && AU_personalid.getLength()<=10 &&
                AU_phonenum.getText().matches(regex_only_numbers) && AU_phonenum.getLength()<=12))
        {
            //evrything passed
        }
        else{
            showAlertError("Some of input is invalid.");
            return;
        }

        if ((Integer.parseInt(AU_acceslevel.getText())==0) || Integer.parseInt(AU_acceslevel.getText())==1 || Integer.parseInt(AU_acceslevel.getText())==2) {
            //access level passed
            if (AU_male.isSelected()) {
                sex = "M";
            }
            if (AU_female.isSelected()) {
                sex = "F";
            }
        }
        else {
            showAlertError("Access level does not match requirements.");
            return;
        }

        if (CardID<0 && LoginID<0) {
        }
        else {
            showAlertError("Card or username already exists.");
            return;
        }
        //endregion

        //region Card
        CardEntity newCardEntity = new CardEntity();
        newCardEntity.setCardnumber(AU_cardnum.getText());
        EntityManagerStuff(newCardEntity);
        CardID = newCardEntity.getCardid();
        LOGGER.info("<<< CardEntity has been created: CardID=" + CardID + ", CardNumber=" + AU_cardnum.getText() + "");
        //endregion

        //region Login
        LoginEntity newLoginEntity = new LoginEntity();
        newLoginEntity.setUsername(AU_username.getText());
        newLoginEntity.setPassword(cryptWithMD5(AU_password.getText()));
        EntityManagerStuff(newLoginEntity);
        LoginID = newLoginEntity.getLoginid();
        LOGGER.info("<<< LoginEntity has been created: LoginID=" + LoginID + ", Username=" + AU_username.getText() + "");
        //endregion

        //region Address
        if (AddrID < 0)
        {
            PersonAddressEntity newAddressEntity = new PersonAddressEntity();
            newAddressEntity.setHouseNumber(AU_housenum.getText());
            newAddressEntity.setZipCode(AU_zipcode.getText());
            newAddressEntity.setStreet(AU_street.getText());
            newAddressEntity.setCity(AU_city.getText());
            EntityManagerStuff(newAddressEntity);
            AddrID = newAddressEntity.getPersonAddressid();
        }
        //endregion

        //region Person
        try {
            PersonEntity newPerson = new PersonEntity();
            newPerson.setCardid(CardID);
            newPerson.setEmail(AU_email.getText());
            newPerson.setEmployeeId(AU_employeeid.getText());
            newPerson.setFirstName(AU_firstname.getText());
            newPerson.setLastName(AU_surname.getText());
            newPerson.setLoginid(LoginID);
            newPerson.setActive("1");
            newPerson.setSex(sex);
            newPerson.setPersonAddressid(AddrID);
            newPerson.setPersonalId(AU_personalid.getText());
            newPerson.setPhoneNumber(AU_phonenum.getText());
            EntityManagerStuff(newPerson);
            PersonID = newPerson.getPersonid();
        }
        catch (RuntimeException e) {
            showAlertError("Some user values alredy exists in database." +
                    "(Phone number, Personal ID, Employee ID, Email)");
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
            return;
        }
        //endregion

        //region Access
        if(AU_day_mon.isSelected()) {
            int AccessIDmon = checkAccessIfExists(AU_acceslevel.getText(), "Mon", mon_from.getText(), mon_to.getText());
            if (AccessIDmon<0) {
                insertToAccess(AU_acceslevel.getText(), "Mon", mon_from.getText(), mon_to.getText(), PersonID);
            }
            else {
                insertToJoinPersonToAccess(PersonID, AccessIDmon);
            }
        }
        if(AU_day_tue.isSelected()) {
            int AccessIDtue = checkAccessIfExists(AU_acceslevel.getText(), "Tue", tue_from.getText(), tue_to.getText());
            if (AccessIDtue<0) {
                insertToAccess(AU_acceslevel.getText(), "Tue", tue_from.getText(), tue_to.getText(), PersonID);
            }
            else {
                insertToJoinPersonToAccess(PersonID, AccessIDtue);
            }
        }
        if(AU_day_wed.isSelected()) {
            int AccessIDwed = checkAccessIfExists(AU_acceslevel.getText(), "Wed", wed_from.getText(), wed_to.getText());
            if (AccessIDwed<0) {
                insertToAccess(AU_acceslevel.getText(), "Wed", wed_from.getText(), wed_to.getText(), PersonID);
            }
            else {
                insertToJoinPersonToAccess(PersonID, AccessIDwed);
            }
        }
        if(AU_day_thu.isSelected()) {
            int AccessIDthu = checkAccessIfExists(AU_acceslevel.getText(), "Thu", thu_from.getText(), thu_to.getText());
            if (AccessIDthu<0) {
                insertToAccess(AU_acceslevel.getText(), "Thu", thu_from.getText(), thu_to.getText(), PersonID);
            }
            else {
                insertToJoinPersonToAccess(PersonID, AccessIDthu);
            }
        }
        if(AU_day_fri.isSelected()) {
            int AccessIDfri = checkAccessIfExists(AU_acceslevel.getText(), "Fri", fri_from.getText(), fri_to.getText());
            if (AccessIDfri<0) {
                insertToAccess(AU_acceslevel.getText(), "Fri", fri_from.getText(), fri_to.getText(), PersonID);
            }
            else {
                insertToJoinPersonToAccess(PersonID, AccessIDfri);
            }
        }
        if(AU_day_sat.isSelected()) {
            int AccessIDsat = checkAccessIfExists(AU_acceslevel.getText(), "Sat", sat_from.getText(), sat_to.getText());
            if (AccessIDsat<0) {
                insertToAccess(AU_acceslevel.getText(), "Sat", sat_from.getText(), sat_to.getText(), PersonID);
            }
            else {
                insertToJoinPersonToAccess(PersonID, AccessIDsat);
            }
        }
        if(AU_day_sun.isSelected()) {
            int AccessIDsun = checkAccessIfExists(AU_acceslevel.getText(), "Sun", sun_from.getText(), sun_to.getText());
            if (AccessIDsun<0) {
                insertToAccess(AU_acceslevel.getText(), "Sun", sun_from.getText(), sun_to.getText(), PersonID);
            }
            else {
                insertToJoinPersonToAccess(PersonID, AccessIDsun);
            }
        }
        //endregion

        //region Clear TextFields
        AU_firstname.clear();
        AU_surname.clear();
        AU_phonenum.clear();
        AU_personalid.clear();
        AU_employeeid.clear();
        AU_email.clear();

        AU_city.clear();
        AU_housenum.clear();
        AU_zipcode.clear();
        AU_street.clear();

        AU_username.clear();
        AU_password.clear();

        AU_acceslevel.clear();
        AU_cardnum.clear();
        AU_day_mon.setSelected(false);
        AU_day_tue.setSelected(false);
        AU_day_wed.setSelected(false);
        AU_day_thu.setSelected(false);
        AU_day_fri.setSelected(false);
        AU_day_sat.setSelected(false);
        AU_day_sun.setSelected(false);

        AU_male.setSelected(false);
        AU_female.setSelected(false);

        //endregion

        em.clear();
    }

    /**
     * Setting visible form to admin to add user
     * @param event - clicking to person add button
     */
    @FXML
    void button_adminAddUser(ActionEvent event) {
        adminLogs.setVisible(false);
        adminUsers.setVisible(false);
        update_pane.setVisible(false);
        adminAddUser.setVisible(true);

        update_accessmon.clear();
        update_accesstue.clear();
        update_accesswed.clear();
        update_accessthu.clear();
        update_accessfri.clear();
        update_accesssat.clear();
        update_accesssun.clear();

        update_lvlmon.clear();
        update_lvltue.clear();
        update_lvlwed.clear();
        update_lvlthu.clear();
        update_lvlfri.clear();
        update_lvlsat.clear();
        update_lvlsun.clear();
    }

    /**
     * Setting visible form to admin to see all user
     * @param event - clicking to see users button
     */
    @FXML
    void button_adminUsers(ActionEvent event) {
        adminAddUser.setVisible(false);
        adminLogs.setVisible(false);
        update_pane.setVisible(false);
        adminUsers.setVisible(true);

        update_accessmon.clear();
        update_accesstue.clear();
        update_accesswed.clear();
        update_accessthu.clear();
        update_accessfri.clear();
        update_accesssat.clear();
        update_accesssun.clear();

        update_lvlmon.clear();
        update_lvltue.clear();
        update_lvlwed.clear();
        update_lvlthu.clear();
        update_lvlfri.clear();
        update_lvlsat.clear();
        update_lvlsun.clear();

        LOGGER.info("<<< Admin requires to see all users");
        query2 = em.createQuery("from PersonEntity");
        List<PersonEntity> listPerson = query2.getResultList();
        ObservableList<PersonEntity> proListPerson = FXCollections.observableArrayList(listPerson);
        upFirstName.setCellValueFactory(new PropertyValueFactory<PersonEntity, String>("firstName"));
        upSurname.setCellValueFactory(new PropertyValueFactory<PersonEntity, String>("lastName"));
        upEmployeeID.setCellValueFactory(new PropertyValueFactory<PersonEntity, String>("employeeId"));
        upActive.setCellValueFactory(new PropertyValueFactory<PersonEntity, String>("active"));
        tableUpdate.setItems(null);
        tableUpdate.setItems(proListPerson);
    }

    /**
     * Setting visible form to admin to see users logs
     * @param event - clicking to see user accesses button
     */
    @FXML
    void button_adminLogs(ActionEvent event) {
        adminAddUser.setVisible(false);
        adminUsers.setVisible(false);
        update_pane.setVisible(false);
        adminLogs.setVisible(true);

        update_accessmon.clear();
        update_accesstue.clear();
        update_accesswed.clear();
        update_accessthu.clear();
        update_accessfri.clear();
        update_accesssat.clear();
        update_accesssun.clear();

        update_lvlmon.clear();
        update_lvltue.clear();
        update_lvlwed.clear();
        update_lvlthu.clear();
        update_lvlfri.clear();
        update_lvlsat.clear();
        update_lvlsun.clear();
    }

    /**
     * View access logs of all users to admin in to table
     */
    @FXML
    void seeAccessLogsAdmin() {

        if(tab_admin.isSelected()){
            LOGGER.info("<<< Admin requires to see all access logs");
            query = em.createQuery("from AccessLogEntity");
            List<AccessLogEntity> list = query.getResultList();
            ObservableList<AccessLogEntity> proList = FXCollections.observableArrayList(list);
            accessDayAdmin.setCellValueFactory(new PropertyValueFactory<AccessLogEntity, Date>("accessday"));
            accessTimeAdmin.setCellValueFactory(new PropertyValueFactory<AccessLogEntity, String>("accesstime"));
            accessResultAdmin.setCellValueFactory(new PropertyValueFactory<AccessLogEntity, Boolean>("accessresult"));
            accessOfficeAdmin.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<AccessLogEntity, String>, ObservableValue<String>>() {
                @Override
                public ObservableValue<String> call(TableColumn.CellDataFeatures<AccessLogEntity, String> param) {
                    return new SimpleStringProperty(param.getValue().getOfficeroomByOfficeroomid().getName());
                }
            });
            accessPersonAdmin.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<AccessLogEntity, String>, ObservableValue<String>>() {
                @Override
                public ObservableValue<String> call(TableColumn.CellDataFeatures<AccessLogEntity, String> param) {
                    return new SimpleStringProperty(param.getValue().getPersonByPersonid().getFirstName() +" "+ param.getValue().getPersonByPersonid().getLastName());
                }
            });

            accessFloorAdmin.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<AccessLogEntity, String>, ObservableValue<String>>() {
                @Override
                public ObservableValue<String> call(TableColumn.CellDataFeatures<AccessLogEntity, String> param) {
                    return new SimpleStringProperty(param.getValue().getOfficeroomByOfficeroomid().getDepartmentByDepartmentid().getFloorByFloorid().getFloornumber());
                }
            });

            tableLogsAdministrator.setItems(null);
            tableLogsAdministrator.setItems(proList);
        }
    }

    /**
     * Initialize setting for the controller
     */
    @FXML
    public void initialize() {
        //All of DOOR buttons
        Button[] Buttons = {
                Floor1_1_Office1_1_BTN,
                Floor1_2_Office2_0_BTN,
                Floor1_3_Office3_2_BTN,
                Floor1_4_Office4_1_BTN,
                Floor1_5_Office5_0_BTN,
                Floor1_6_ConferenceRoom1_0_BTN,
                Floor1_7_BreakRoom_2_BTN,
                Floor2_8_Office21_0_BTN,
                Floor2_9_Office22_2_BTN,
                Floor2_10_Office23_1_BTN,
                Floor2_11_Office24_2_BTN,
                Floor2_12_Office25_0_BTN,
                Floor2_13_ConferenceRoom21_0_BTN,
                Floor2_14_Office26_0_BTN,
                Floor1_15_Entry_2_BTN,
                Floor1_1_Office1_1_BTN2,
                Floor1_2_Office2_0_BTN2,
                Floor1_3_Office3_2_BTN2,
                Floor1_4_Office4_1_BTN2,
                Floor1_5_Office5_0_BTN2,
                Floor1_6_ConferenceRoom1_0_BTN2,
                Floor1_7_BreakRoom_2_BTN2,
                Floor2_8_Office21_0_BTN2,
                Floor2_9_Office22_2_BTN2,
                Floor2_10_Office23_1_BTN2,
                Floor2_11_Office24_2_BTN2,
                Floor2_12_Office25_0_BTN2,
                Floor2_13_ConferenceRoom21_0_BTN2,
                Floor2_14_Office26_0_BTN2,
                Floor1_15_Entry_2_BTN2
        };

        //AU_accesslevel textfield is NUMERICAL ONLY
        AU_acceslevel.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*")) {
                    AU_acceslevel.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });

        //SETS same click action for every door button we have
        for(int i = 0; i < Buttons.length; i++) {
            int thisButton = i;
            //WHEN BUTTON IS CLICKED
            Buttons[i].setOnAction(f -> {
                boolean locked = true; //office is locked

                //These will be INSERTED to access_log table...HANDFILLED
                String OfficeName = "default";
                String FloorName = "default";
                int OfficeRoomId = -1;
                int LevelRequired;

                /*
                * Get name of the button so we can parse it
                * Name is in this format: Floor2_14_Office26_2_BTN
                * When we parse it we get following information:
                * FloorName = Floor2
                * OfficeID = 14
                * OfficeName = Office26
                * LevelRequired = 2*/
                String s = ((Button) f.getSource()).getId();
                String[] tokens = s.split("_");

                int count = 0;
                //Filling the variables with parsed info
                for (String t : tokens){
                    count++;
                    switch (count){
                        case 1:
                            FloorName = t;
                            break;
                        case 2:
                            OfficeRoomId = Integer.parseInt(t);
                            break;
                        case 3:
                            OfficeName = t;
                            break;
                        case 4:
                            LevelRequired = Integer.parseInt(t);
                            locked = accessCheck(LevelRequired); //CALLING ACCESS CHECK
                            break;
                    }
                }
                //If datepick was not set return from this function
                if(datePickSet == -1){
                    return;
                }

                //This thread makes button blink
                ChangeColorThread changeColor = new ChangeColorThread(Buttons[thisButton] , locked, OfficeName, FloorName);
                changeColor.start();

                //Insert to access log - only if we are doing it on live access tab
                if(liveAccessTab.isSelected()){
                    insertAccessLog(currentDate, currentTime, !locked, OfficeRoomId); //Notice we have to pass negation of locked
                }
            });
        }

        label_Hello.setText("   Hello "+getUsername());
        ChangeColorThread.LoggerInit(); //Initialize logger for ChangeColorThread

        try {
            // This block configure the logger with handler and formatter
            fileHandler = new FileHandler(LOGS_FOLDER+"JavaFxController.log", true);
            LOGGER.addHandler(fileHandler);
            SimpleFormatter formatter = new SimpleFormatter();
            fileHandler.setFormatter(formatter);
            LOGGER.setLevel(Level.INFO);
            LOGGER.info("<<< Logging of " +JavaFxController.class.getName()+ " initiated successfully");

        } catch (SecurityException e) {
            e.printStackTrace();
            LOGGER.log( Level.SEVERE, e.toString(), e); //Always logging exceptions
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.log( Level.SEVERE, e.toString(), e); //Always logging exceptions
        }

        try {
            timeSelector.setTextFormatter(new TextFormatter(new DateTimeStringConverter(sdf), sdf.parse("00:00")));
            mon_from.setTextFormatter(new TextFormatter(new DateTimeStringConverter(sdf), sdf.parse("00:00")));
            mon_to.setTextFormatter(new TextFormatter(new DateTimeStringConverter(sdf), sdf.parse("00:00")));
            tue_from.setTextFormatter(new TextFormatter(new DateTimeStringConverter(sdf), sdf.parse("00:00")));
            tue_to.setTextFormatter(new TextFormatter(new DateTimeStringConverter(sdf), sdf.parse("00:00")));
            wed_from.setTextFormatter(new TextFormatter(new DateTimeStringConverter(sdf), sdf.parse("00:00")));
            wed_to.setTextFormatter(new TextFormatter(new DateTimeStringConverter(sdf), sdf.parse("00:00")));
            thu_from.setTextFormatter(new TextFormatter(new DateTimeStringConverter(sdf), sdf.parse("00:00")));
            thu_to.setTextFormatter(new TextFormatter(new DateTimeStringConverter(sdf), sdf.parse("00:00")));
            fri_from.setTextFormatter(new TextFormatter(new DateTimeStringConverter(sdf), sdf.parse("00:00")));
            fri_to.setTextFormatter(new TextFormatter(new DateTimeStringConverter(sdf), sdf.parse("00:00")));
            sat_from.setTextFormatter(new TextFormatter(new DateTimeStringConverter(sdf), sdf.parse("00:00")));
            sat_to.setTextFormatter(new TextFormatter(new DateTimeStringConverter(sdf), sdf.parse("00:00")));
            sun_from.setTextFormatter(new TextFormatter(new DateTimeStringConverter(sdf), sdf.parse("00:00")));
            sun_to.setTextFormatter(new TextFormatter(new DateTimeStringConverter(sdf), sdf.parse("00:00")));
        } catch (ParseException e) {
            e.printStackTrace();
            LOGGER.log( Level.SEVERE, e.toString(), e); //Always logging exceptions
        }
    }

    /**
     * Checks if user is admin or not and grants privileges based on result
     * @param event - clicking the button with shield picture
     */
    @FXML
    void admin_check(ActionEvent event){
        Alert alert;

        try {
            //Is the user admin?
            if(Db.isUserAdmin()){
                tab_pane.managedProperty().bind(tab_pane.disabledProperty());
                tab_admin.setDisable(false);

                button_adminPicture.setDisable(true);
                button_adminPicture2.setDisable(true);

                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText("Administrator privileges granted!");
                alert.showAndWait();
            }
            else{
                alert = new Alert(Alert.AlertType.WARNING);
                alert.setHeaderText("This is not account with administrator privileges.");
                alert.showAndWait();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
        }
    }

    /**
     * Closes the entity manager and exists the APP
     * @param event - clicking the button
     */
    @FXML
    void button_exit(ActionEvent event) {
        em.close();
        emf.close();
        System.exit(0);
    }
}

