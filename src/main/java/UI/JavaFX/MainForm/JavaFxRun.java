package UI.JavaFX.MainForm;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.effect.Reflection;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import static Main.App.LOGS_FOLDER;

public class JavaFxRun extends Application {

    //region LOGGER + filehandler
    private final static Logger LOGGER = Logger.getLogger(JavaFxRun.class.getName());
    //region Entity Manager
    public static EntityManagerFactory emf;
    public static EntityManager em;
    //endregion
    private static FileHandler fileHandler;
    //endregion

    private Stage loadingStage; //Stage, which contains loading gif


    /**
     * Main javaFX method, when executed it prepares the UI setup and initialize Controller
     * @param args - console arguments
     */
    public static void runFX(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        try {
            // This block configure the logger with handler and formatter
            fileHandler = new FileHandler(LOGS_FOLDER + "JavaFxRun.log", true);
            LOGGER.addHandler(fileHandler);
            SimpleFormatter formatter = new SimpleFormatter();
            fileHandler.setFormatter(formatter);
            LOGGER.setLevel(Level.INFO);
            LOGGER.info("<<< Logging of " +JavaFxRun.class.getName()+ " initiated successfully");

        } catch (SecurityException e) {
            e.printStackTrace();
            LOGGER.log( Level.SEVERE, e.toString(), e); //Always logging exceptions
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.log( Level.SEVERE, e.toString(), e); //Always logging exceptions
        }


        loadingStage = primaryStage;

        //region LOADING SCREEN
        Group loadingRoot = new Group();
        Image image = new Image(getClass().getResourceAsStream("/loading.gif"));
        ImageView view = new ImageView(image);
        loadingRoot.getChildren().add(view);
        view.setOnMouseClicked(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent t){
                //System.exit(0);
            }
        });

        view.setEffect(new Reflection());
        Scene scene = new Scene(loadingRoot, image.getWidth(), 2*image.getHeight(), Color.TRANSPARENT);

        loadingStage.initStyle(StageStyle.TRANSPARENT); //Remove decoration
        loadingStage.setScene(scene);
        loadingStage.show(); //show the loadingStage
        //endregion




        Parent root = FXMLLoader.load(getClass().getResource("/MainForm.fxml"));
        //region TASK to kick in entity manager
        Task<Void> task = new Task<Void>() {
            @Override
            public Void call() throws Exception {
                try {
                    LOGGER.info("<<< Thread for entity manager started.");
                    emf = Persistence.createEntityManagerFactory("NewPersistenceUnit");
                    em = emf.createEntityManager();
                    LOGGER.info("<<< Entity manager loaded successfully");
                    return null;
                }
                catch (RuntimeException e){
                    //In case of TOO MANY CONNECTIONS --> EXIT
                    LOGGER.log( Level.SEVERE, e.toString(), e); //Always logging exceptions
                    Platform.exit();
                    System.exit(0);
                }
                return null;
            }
        };
        //After entity manager loads, open mainStage
        task.setOnSucceeded(e -> {

            Stage mainStage = new Stage();
            mainStage.setTitle("BACS");
            mainStage.initStyle(StageStyle.DECORATED);
            mainStage.setScene(new Scene(root, 800, 600));

            mainStage.getIcons().add(new Image(JavaFxRun.class.getResourceAsStream("/miniIcon.png" ))); //Setting up small icon for app
            mainStage.setResizable(false); //We don't want user to resize the app

            mainStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent t) {
                    cleanUpAndExit();
                }
            });

            loadingStage.hide();
            mainStage.show();
        });
        //endregion

        new Thread(task).start(); //Starting new thread and passing in the task we prepared

        loadingStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                cleanUpAndExit();
            }
        });
    }

    /**
     * Closes entity manager and exists application
     */
    private void cleanUpAndExit(){
        em.close();
        emf.close();
        Platform.exit();
        System.exit(0);
    }
}
