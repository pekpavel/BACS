package UI.HandMade;

import Database.Db;
import UI.JavaFX.MainForm.JavaFxRun;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import static Main.App.LOGS_FOLDER;


public class LoginForm {

    //region LOGGER + filhandler
    private final static Logger LOGGER = Logger.getLogger(LoginForm.class.getName());
    private static FileHandler fileHandler;
    //endregion
    //region DATABASE VARIABLES
    Db database;
    //endregion
    //Variables preparation
    //region MAINFRAME
    private JFrame mainFrame;
    //region LABELS
    private JLabel headerLabel;
    private JLabel usernameLabel;
    private JLabel passwordLabel;
    //endregion
    private JLabel pictureLabel;
    //region BUTTONS
    private JButton signInBTN;
    //endregion
    private JButton closeBTN;
    //region TEXTFIELDS
    private JTextField usernameTXT;
    //endregion
    private JTextField passwordTXT;
    //endregion
    //region PANElS
    private JPanel bluePanel;
    private int userID;
    //endregion

    /**
     * Constructor for Login Form - prepares GUI, set LOGGER and connects to database
     */
    public LoginForm(){
        prepareGUI();
        try {
            // This block configure the logger with handler and formatter
            fileHandler = new FileHandler(LOGS_FOLDER + "LoginForm.log", true);
            LOGGER.addHandler(fileHandler);
            SimpleFormatter formatter = new SimpleFormatter();
            fileHandler.setFormatter(formatter);
            LOGGER.setLevel(Level.INFO);
            LOGGER.info("<<< Logging of " +LoginForm.class.getName()+ " initiated successfully");

        } catch (SecurityException e) {
            e.printStackTrace();
            LOGGER.log( Level.SEVERE, e.toString(), e); //Always logging exceptions
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.log( Level.SEVERE, e.toString(), e); //Always logging exceptions
        }
        database = new Db(); //Db instance
        database.loadLogger();
    }


    /**
     * Prepares the look of the GUI
     */
    private void prepareGUI(){
        pictureLabel = new JLabel();
        pictureLabel.setIcon(new ImageIcon(getClass().getResource("/login.png"))); //Setting the image to the label
        pictureLabel.setBounds(30, 40, 200, 200);

        mainFrame = new JFrame("BACS - Building access control system");
        mainFrame.setUndecorated(true); //Removing borders of the mainFrame
        mainFrame.setLayout(null);

        //Position and size - centered to the middle of the screen
        mainFrame.setSize(530, 530);
        mainFrame.setLocationRelativeTo(null);

        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Default operation on close

        usernameTXT = new JTextField();
        usernameTXT.setBounds(240, 322, 258, 32); //Position and size

        passwordTXT = new JPasswordField();
        passwordTXT.setBounds(240, 379, 258, 32); //Position and size

        headerLabel = new JLabel();
        headerLabel.setText("<html>Welcome to BACS<br>please sign in to continue!</html>");
        headerLabel.setForeground(Color.WHITE); //Color of the font
        headerLabel.setFont(new Font("Agency FB", Font.BOLD, 30)); //Font
        headerLabel.setBounds(245, 105, 250, 100);


        usernameLabel = new JLabel("Username");
        usernameLabel.setBounds(73, 323, 100, 25); //Position and size
        usernameLabel.setFont(new Font("Sagoe UI", Font.BOLD, 20)); //Font

        passwordLabel = new JLabel("Password");
        passwordLabel.setBounds(73, 383, 100, 25); //Position and size
        passwordLabel.setFont(new Font("Segoe UI", Font.BOLD, 20));

        signInBTN = new JButton("Sign in");
        signInBTN.setBounds(240, 436, 125, 70); //Position and size

        //SignInBTN CLICK
        signInBTN.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                database.openConnection(); //Opens connection

                //If CONNECTION to database is OK
                if(database.getRetConnection() == 0){
                    try {
                            userID = database.checkUser(usernameTXT.getText(), passwordTXT.getText());

                            //If we found the user
                            if(userID != -1){
                                LOGGER.info("<<< USER: " + usernameTXT.getText() + " signed in");
                                mainFrame.setVisible(false); //Hiding the login frame
                                JavaFxRun.runFX(null); //Running javaFX
                            }

                            else{
                                JOptionPane.showMessageDialog(new JFrame(), "Incorrect combination of username and password \nor maybe your account is inactive, in this case please contact the administrator." , "Warning",
                                        JOptionPane.WARNING_MESSAGE);
                                LOGGER.warning("### User entered incorrect combination of username and password");
                            }

                    } catch (SQLException e1) {
                        e1.printStackTrace();
                        LOGGER.log( Level.SEVERE, e1.toString(), e1); //Always logging exceptions
                    }

                    finally {
                        database.closeConnection(); //Closing the connection to db
                    }
                }

                else{
                    //Show error message
                    JOptionPane.showMessageDialog(new JFrame(), "Connection to database is not available, please try it later.", "Problem with connection",
                            JOptionPane.ERROR_MESSAGE);
                    LOGGER.log( Level.SEVERE, "### Connection to database failed."); //Always logging exceptions
                }
            }
        });

        closeBTN = new JButton("Close");
        closeBTN.setBounds(372, 436, 125, 70); //Position and size

        //CloseBTN CLICK
        closeBTN.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
                //If database is connected, disconnect it
                if(database.getRetConnection() == 0){
                    database.closeConnection(); //Closing the connection to db
                }

                LOGGER.info("<<< Application is about to EXIT");
                System.exit(0);
            }
        });

        bluePanel = new JPanel();
        bluePanel.setBackground(Color.BLUE);
        bluePanel.setBounds(0, 0, 530, 285); //Position and size
        bluePanel.setLayout(null);

        //region ADDING
        bluePanel.add(pictureLabel);
        bluePanel.add(headerLabel);

        mainFrame.add(usernameLabel);
        mainFrame.add(passwordLabel);
        mainFrame.add(signInBTN);
        mainFrame.add(closeBTN);
        mainFrame.add(usernameTXT);
        mainFrame.add(passwordTXT);
        mainFrame.add(bluePanel);
        //endregion

        mainFrame.getRootPane().setDefaultButton(signInBTN); //signInBTN listens on ENTER key
        mainFrame.setVisible(true); //Viewing the mainFrame
    }
}
