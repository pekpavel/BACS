package Main;

import UI.HandMade.LoginForm;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;


public class App
{
    //Public folder variable
    public final static String LOGS_FOLDER = "log/";
    //region LOGGER + fileHandler
    private final static Logger LOGGER = Logger.getLogger(App.class.getName());
    //endregion
    private static FileHandler fileHandler;

    /**
     * STARTING POINT OF THE PROGRAM
     * @param args - from console
     */
    public static void main( String[] args ){
        try {
            // This block configure the logger with handler and formatter
            fileHandler = new FileHandler(LOGS_FOLDER + "App.log", true);
            LOGGER.addHandler(fileHandler);
            SimpleFormatter formatter = new SimpleFormatter();
            fileHandler.setFormatter(formatter);
            LOGGER.setLevel(Level.INFO);
            LOGGER.info("<<< Logging of " +App.class.getName()+ " initiated successfully");

        } catch (SecurityException e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.toString(), e); //Always logging exceptions
        }

        LoginForm loginForm = new LoginForm(); //Creating instance of logging form --> opens Login Form
        LOGGER.info("<<< Application has started");
    }

}
