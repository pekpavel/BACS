package UI.JavaFX.MainForm;

import DBModels.AccessLogEntity;
import DBModels.CardEntity;
import DBModels.LoginEntity;
import DBModels.PersonEntity;
import Database.Db;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import javax.persistence.Persistence;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static Database.CryptWithMD5.cryptWithMD5;


/**
 * Created by Pavel Pek on 21.05.2017.
 */
@RunWith(Enclosed.class)
public class JavaFxControllerTest {
    //Variables
    static Db databaseInstance;
    static int userID;
    static JavaFxController newControllerInstance;
    static JavaFxRun javaFxRun;
    static AccessLogEntity lastAccessLog;

    //PARAMETRIZED CLASS
    @RunWith(Parameterized.class)
    public static class TheParameterizedPart{

        static int countTests = 1; //Counting tests run
        String InputParam1, InputParam2, InputParam3, InputParam4; //Input variables

        /**
         * Costructor - sets input parameters
         * @param InputParam1 - input parameter taken from Collection
         * @param InputParam2 - input parameter taken from Collection
         * @param InputParam3 - input parameter taken from Collection
         * @param InputParam4 - input parameter taken from Collection
         */
        public TheParameterizedPart(String InputParam1, String InputParam2, String InputParam3, String InputParam4){
            this.InputParam1 = InputParam1;
            this.InputParam2 = InputParam2;
            this.InputParam3 = InputParam3;
            this.InputParam4 = InputParam4;
        }

        @Parameterized.Parameters
        public static Collection<Object[]> data() {
            Object[][] data = {{"Brno", "224", "61400", "Ruzova"},           //valid data - should return positive INTEGER (greater than -1)
                               {"Stoka", "777", "77777", "xxxx"}};           //invalid data - should return -1
            return Arrays.asList(data);
        }

        @Test
        public void checkAddressIfExistsTest(){
            //TEST CASE 1 - should find valid address
            if(countTests == 1){
                Assert.assertNotEquals("This address does not exist in database!", -1, newControllerInstance.checkAddressIfExistsCall(InputParam1, InputParam2, InputParam3, InputParam4));
                countTests++;
            }
            //TEST CASE 2 - should not find valid address
            else if(countTests == 2){
                Assert.assertEquals("This address exists in database!", -1, newControllerInstance.checkAddressIfExistsCall(InputParam1, InputParam2, InputParam3, InputParam4));
                countTests++;
            }
        }
    }

    //NOT PARAMETRIZED CLASS
    public static class NotParametrizedPart {
        @BeforeClass
        public static void setUp() throws Exception {
            databaseInstance = new Db();
            databaseInstance.openConnection(); //Open conn
            userID = databaseInstance.checkUser("honza", "honza"); //Expect ID 2

            //Instances
            newControllerInstance = new JavaFxController();
            javaFxRun = new JavaFxRun();

            //Entity manager - takes long to create
            JavaFxRun.emf = Persistence.createEntityManagerFactory("NewPersistenceUnit");
            JavaFxRun.em = JavaFxRun.emf.createEntityManager();
        }

        @AfterClass
        public static void tearDown() throws Exception {
            databaseInstance.closeConnection(); //Close connection
            //Clean up
            JavaFxRun.em.clear();
            JavaFxRun.em.close();
            JavaFxRun.emf.close();
        }

        /*
        * If connection opened successfully in setUp method, getRetConnection() method must return 0 - that signalizes OK connection
        */
        @Test
        public void openConnectionTest(){
            Assert.assertEquals("Connection to database is not open", 0, databaseInstance.getRetConnection());
        }

        /**
         * Inserts new access log to the access_log table, then compares if last added access log is the same...if insert works properly, they should be the same.
         * Removes the access log so it does not stay in database.
         */
        @Test
        public void insertAccessLogTest(){
            //We want to create access log with these information
            String Accessday = "21.05.17";
            String Accesstime = "22:00";
            int Personid = 2; //Must match our user id
            boolean result = true;

            //Create access log with expected information
            AccessLogEntity original = new AccessLogEntity();
            original.setAccessday(Accessday);
            original.setAccesstime(Accesstime);
            original.setPersonid(Personid);
            original.setAccessresult(result);

            //Insert the same values to the database table
            newControllerInstance.insertAccessLog(Accessday, Accesstime, result, 1);
            JavaFxRun.em.clear();
            //Select last access log added
            List<AccessLogEntity> lastRecord = JavaFxRun.em.createQuery("from AccessLogEntity order by accessLogid DESC").setMaxResults(1).getResultList();
            lastAccessLog = lastRecord.get(0); //Save it to AccessLogEntity datatype
            Assert.assertEquals(true, original.equals(lastAccessLog)); //Compare it

            //Remove last accessLog
            JavaFxRun.em.getTransaction().begin();
            JavaFxRun.em.remove(lastAccessLog);
            JavaFxRun.em.getTransaction().commit();
        }

        /**
         * Checks if user defined above is admin or not.
         * @throws SQLException
         */
        @Test
        public void isUserAdminTest() throws SQLException {
            Assert.assertEquals("This user is admin!!!", false, Db.isUserAdmin()); //We know we are on account "honza"..that is not administrator
        }

        /**
         * Creates new card, checks if it exists, removes it and checks if it does not exist anymore.
         */
        @Test
        public void CheckCardIfExistsTest(){
            String newCardNum = "25896325";
            //Create new card with number 25896325
            CardEntity newCardEntity = new CardEntity();
            newCardEntity.setCardnumber(newCardNum);
            newControllerInstance.EntityManagerStuff(newCardEntity);
            int CardID = newCardEntity.getCardid();

            //Check if card exists - it should now.
            Assert.assertEquals("Card with this number does not exist!!!", CardID, newControllerInstance.checkCardIfExistsCall(newCardNum));

            //Remove the card
            JavaFxRun.em.getTransaction().begin();
            JavaFxRun.em.remove(newCardEntity);
            JavaFxRun.em.getTransaction().commit();

            Assert.assertEquals("Card with this number still exists!!!", -1, newControllerInstance.checkCardIfExistsCall(newCardNum));
        }

        /**
         * Activates or deactivates Honza, depending on the actual value
         */
        @Test
        public void adminDeactivateOrActivateUserTest(){
            PersonEntity honza = JavaFxRun.em.getReference(PersonEntity.class, 2);
            //ACTIVATE
            if (honza.getActive().equals("0")) {
                JavaFxRun.em.clear();
                JavaFxRun.em.getTransaction().begin();
                honza.setActive("1");
                JavaFxRun.em.merge(honza);
                JavaFxRun.em.getTransaction().commit();
                JavaFxRun.em.clear();

                Assert.assertEquals(honza.getActive(), "1");
            }
            //DEACTIVATE
            else {
                JavaFxRun.em.clear();
                JavaFxRun.em.getTransaction().begin();
                honza.setActive("0");
                JavaFxRun.em.merge(honza);
                JavaFxRun.em.getTransaction().commit();
                JavaFxRun.em.clear();

                Assert.assertEquals(honza.getActive(), "0");
            }

            //In the END I want honza to be set on active
            JavaFxRun.em.clear();
            JavaFxRun.em.getTransaction().begin();
            honza.setActive("1");
            JavaFxRun.em.merge(honza);
            JavaFxRun.em.getTransaction().commit();
            JavaFxRun.em.clear();
            Assert.assertEquals(honza.getActive(), "1");
        }

        /**
         * Creates new login data, checks if it exists, removes it and checks if it does not exist anymore.
         */
        @Test
        public void CheckLoginIfExistsTest(){
            String username = "forTest";
            String password = "forTest";

            LoginEntity newLoginEntity = new LoginEntity();
            newLoginEntity.setUsername(username);
            newLoginEntity.setPassword(cryptWithMD5(password));
            newControllerInstance.EntityManagerStuff(newLoginEntity);
            int LoginID = newLoginEntity.getLoginid();

            //Check if login exists - it should now.
            Assert.assertEquals("Login with this username does not exist!!!", LoginID, newControllerInstance.checkLoginIfExistsCall(username));

            //Remove the login
            JavaFxRun.em.getTransaction().begin();
            JavaFxRun.em.remove(newLoginEntity);
            JavaFxRun.em.getTransaction().commit();

            Assert.assertEquals("Login with this username still exists!!!", -1, newControllerInstance.checkLoginIfExistsCall(username));
        }

        /*
        * Checks if PersonalID exists, we will take this account's personalID
        * */
        @Test
        public void checkPersonalidIfExists(){
            Assert.assertNotEquals("This personal ID does not exist in database!!!", -1, newControllerInstance.checkPersonlidIfExistsCall("8404102458"));
        }
    }
}