package Database;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

/**
 * Created by Pavel Pek on 22.05.2017.
 */
@RunWith(Parameterized.class)
public class CryptWithMD5Test {
    String InputParam1, InputParam2;

    /**
     * Costructor - sets input parameters
     * @param InputParam1 - input parameter taken from Collection
     * @param InputParam2 - input parameter taken from Collection
     */
    public CryptWithMD5Test(String InputParam1, String InputParam2){
        this.InputParam1 = InputParam1;
        this.InputParam2 = InputParam2;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = {{"honza","d9479746ea51a4177f8bc092c5db7b8d"},
                           {"heslo","955db0b81ef1989b4a4dfeae8061a9a6"},
                           {"superPassw:rd","f9b64f42126e644da24221b8a096a383"}};
        return Arrays.asList(data);
    }

    @Test
    public void cryptWithMD5Test() throws Exception {
        Assert.assertEquals("These hashes don't match.", InputParam2, CryptWithMD5.cryptWithMD5(InputParam1));
    }
}