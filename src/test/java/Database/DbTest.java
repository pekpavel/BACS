package Database;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class DbTest {
    static int countTests = 1; //Counting tests run
    Db newDbInstance = new Db(); //Instance of DB
    String InputParam1, InputParam2;

    /**
     * Costructor - sets input parameters
     * @param InputParam1 - input parameter taken from Collection
     * @param InputParam2 - input parameter taken from Collection
     */
    public DbTest(String InputParam1, String InputParam2){
        this.InputParam1 = InputParam1;
        this.InputParam2 = InputParam2;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = {{"honza","honza"},           //valid data - should return positive INTEGER (greater than -1)
                          {"////sadad","-578kn//*"},    //invalid data - should return -1
                          {"",""},                      //empty data - should return -1
                          {"honza",""},                 //username correct, password empty - should return -1
                          {"","honza"},                 //password correct, username empty - should return -1
                          {"katkapo","katkapo"}};       //INACTIVE account - should return -1
        return Arrays.asList(data);
    }

    @Before
    public void setUp() throws Exception {
        newDbInstance.openConnection();
    }

    @After
    public void tearDown() throws Exception {
        newDbInstance.closeConnection();
    }

    @Test
    public void checkUserWithValidData() throws Exception {

        System.out.println("Param1: " + this.InputParam1);
        System.out.println("Param2: " + this.InputParam2);

        int result = newDbInstance.checkUser(this.InputParam1, this.InputParam2); //Passing input parameters
        //TEST CASE 1 - valid data - should return positive INTEGER (greater than -1)
        if(countTests == 1){
            Assert.assertNotEquals(-1, result);
            countTests++;
        }
        //TEST CASE 2 - invalid data - should return -1
        else if(countTests == 2){
            Assert.assertEquals(-1, result);
            countTests++;
        }
        //TEST CASE 3 - empty data - should return -1
        else if(countTests == 3){
            Assert.assertEquals(-1, result);
            countTests++;
        }
        //TEST CASE 4 - username correct, password empty - should return -1
        else if(countTests == 4){
            Assert.assertEquals(-1, result);
            countTests++;
        }
        //TEST CASE 5 - password correct, username empty - should return -1
        else if(countTests == 5){
            Assert.assertEquals(-1, result);
            countTests++;
        }
        //TEST CASE 6 - INACTIVE account - should return -1
        else if(countTests == 6){
            Assert.assertEquals(-1, result);
            countTests++;
        }
    }
}