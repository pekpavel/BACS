package DBModels;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

/**
 * Created by Pavel Pek on 22.05.2017.
 */
@RunWith(Parameterized.class)
public class AccessLogEntityTest {
    static int countTests = 1; //Counting tests run
    String InputParam1, InputParam2;
    /**
     * Costructor - sets input parameters
     * @param InputParam1 - input parameter taken from Collection
     * @param InputParam2 - input parameter taken from Collection
     */
    public AccessLogEntityTest(String InputParam1, String InputParam2){
        this.InputParam1 = InputParam1;
        this.InputParam2 = InputParam2;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = {{"22.05.17","Mon"},
                {"02.05.17","Tue"},
                {"30.12.2016","Fri"}};
        return Arrays.asList(data);
    }

    /**
     * Should convert date to day name
     * @throws Exception
     */
    @Test
    public void convertDateToDayTest() throws Exception {
        Assert.assertEquals("This day does not match with this date.", InputParam2, AccessLogEntity.convertDateToDay(InputParam1));
    }
}