CREATE FUNCTION checkaccess () RETURNS trigger
	LANGUAGE plpgsql
AS $$
DECLARE
  row Access%ROWTYPE;
BEGIN
  FOR row in SELECT * FROM access
  LOOP
    IF((row.accessdayallowed = NEW.accessdayallowed)
       AND (row.accesstimefromto = new.accesstimefromto)
       AND (row.accesslevel = new.accesslevel))
    THEN RAISE EXCEPTION 'Access time already exists.';
    END IF;
  END LOOP;
  RETURN new;
END;
$$
