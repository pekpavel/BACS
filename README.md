Building Access Control System
==============================

V grafickém rozhraní vytvoříme jakýsi model budovy. Model bude obsahovat kompletní strukturu budovy tzn. jednotlivá patra, místnosti, kanceláře apod.
Jako uživatel budete mít svoje user id(UID) a na základě vaší pozice ve firmě vám admin přidělí oprávnění do jednotlivých kanceláří/místností v určitý časový interval. 
Pokud budete chtít jako uživatel vstoupit do nějaké místnosti, systém sám vyhodnotí zda vám přístup povolí, výsledek graficky znázorní a zaloguje. 
Admin bude mít možnost pomocí aplikace měnit oprávnění uživatelů.

Všechna data o uživatelích a místnostech budou uložena v databázi, ke které se bude aplikace připojovat.

========================================================================================================
Stručný návod jak aplikaci obsluhovat:
========================================================================================================
* Při spuštění aplikace je nutné se přihlásit (jste v pozici zaměstnance firmy, který má přístupové údaje již přidělené).
* Systém přihlášení vyhodnocuje samozřejmě zda zadané heslo (v db hashováno pomocí algoritmu MD5, což víme - není téměř vůbec bezpečné, šlo nám jen o to, aby hesla v db nebyly v plain textu) sedí se zadaným uživatelským jménem, ale také zda je zaměstnanec aktivní či ne (v tabulce person sloupec acitve nabývá hodnot 0 - neaktivní, 1 - aktivní). V případě, že aktivní není samozřejmě není možné se do systému přihlásit. Důvod proč jsme se rozhodli do tabulky person přidat sloupec active je následující: vezměme v potaz případ, kdy zaměstnanec dá výpověď a po nějaké době se do firmy bude chtít vrátit zpět. Pokud bychom ho rovnou smazali, musíme celý jeho profil vytvářet znova. Takto je možné nastavit v případě výpovědi sloupec active na hodnotu 0 a v případě návratu zaměstnance ho opět nastavit na 1.
* Přihlašte se prosím pod některým z následujících účtů:
    * **ÚČET 1:**
        * Přihlašovací jméno: admin
        * Heslo: admin
        * Aktivní status: ANO
        * Administrátorský účet: ANO
    * **ÚČET 2:**
        * Přihlašovací jméno: honza
        * Heslo: honza
        * Aktivní status: ANO
        * Administrátorský účet: NE
    * **ÚČET 3:**
        * Přihlašovací jméno: katkapo
        * Heslo: katkapo
        * Aktivní status: NE
        * Administrátorský účet: NE
* V případě, že bude problém s připojením k databázi bude Vám to ohlášeno chybovým message boxem.
* Po úspěšném přihlášení byste měl vidět "Loading" screen. To je z důvodu dlouhého zapínání JPA entity manageru.
* Jakmile "Loading" screen skončí nahcházíte se v hlavním prostřední celého programu. Jak vidíte, okno obsahuje 5 záložek (z toho jedna přístupná pouze pro administrátory).
    * Záložka **Live Access** - simuluje reálný přístup zaměstnance do budovy. Tzn. bere aktuální čas, den a úroveň právě přihlášeného uživatele a po stisku tlačítka dveří vyhodnotí, zda má uživatel v tuto chvíli přístup či ne. Své přístupy můžete vidět na záložce "My accesses". Nutno podotkout, že místnosti jsou označený zkratkou ALR (Access level required) X, kde X nabývá hodnot od 0 do 2. Hodnota 0 je nejvyšší úroveň, hodnota 2 je nejnižší. Když se podíváte na záložku "My accesses" vidíte tam, jakou úroveň přístupu (Access level) v daný den a hodiny máte. Pokud například máte úroveň 1, můžete přistupovat pouze do místností které mají ALR 1 nebo ALR 2.
    * Záložka **Test Access** - je velmi podobná záložce live access nicméně jak již název vypovídá jedná se o záložku sloužící k testovacím účelům tzn. nastavíte si den a čas a můžete zkoušet, zda vás to do příslušných místností pustí či ne.
    * Záložka **Access Logs** - zde je možné se podívat kdy, kam, v jakou hodinu a s jakým výsledkem jste se snažil přistupovat. Logují se pouze přístupy vyvolané na záložce "Live Access".
    * Záložka **My accesses** - přehled Vašich přístupů.
    * Záložka **Admin** /greyed out/ - tato záložka se odemkne v případě, že jste přihlášen pod administrátorským účtem a kliknete na obrázek štítu na záložce "Live Access" nebo na záložce "Test Access".
        * Tato záložka obsahuje logy přístupů VŠECH zaměstnanců.
        * Dále možnost přidání nového zaměstnance.
        * A možnost Updatu, Aktivace/Deaktivace, případně úplné odstranění (před odstraněním musí být zaměstnanec nejdříve uveden do stavu neaktivity) některého ze stávajících zaměstnanců.
         
*Poznámka*: Administrátor je takový zaměstnanec, jehož karta začíná číslem 533xxxxx.